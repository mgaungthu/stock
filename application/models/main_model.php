<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Main_model extends CI_Model{

		function __construct()
	    {
	        // Call the Model constructor
	        parent::__construct();
	        $this->form_validation->set_message ('required','*');
	        $this->form_validation->set_message ('numeric','*');
	        $this->form_validation->set_message ('matches','*');
	        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

			function p_type($value){
				$input = $value;
				if($input==0){
					return '-';
				}
				else{
					$lang_skill = array('New','Return','Return Damage','Return To Repair');
					$digit = array(1,2,3,4);
					return str_replace($digit,$lang_skill,$input);
				}

			}

			function date_time($value)
			{
				date_default_timezone_set("Asia/Rangoon");
				$date_time=date('d-m-Y H:i:s',$value);
				return $date_time;
			}

			function situation($value)
			{
				$input = $value;
				$marital = array('Voucher','Already Sent','Already Called','Can`t Call','Cancel','No Status');
				$digit = array(0,1,2,3,4,9);
				return str_replace($digit,$marital,$input);
			}
	    }
		public function p_type($value)
		{
			$input = $value;
			if ($input == 0) {
				return '-';
			} else {
				$lang_skill = array('New', 'Return', 'Return Damage', 'Return To Repair');
				$digit = array(1, 2, 3, 4);
				return str_replace($digit, $lang_skill, $input);
			}
		}
		
		public function category($value)
		{
			if($value==0){
				return '-';
			}
			else{
				$this->db->where('cid',$value);
				$val=$this->db->get('category_tbl')->row_array();
				return $val['name'];
			}

		}



	   

		public function loginValidate()
		{
			$this->form_validation->set_rules('loginName','','required');
			$this->form_validation->set_rules('loginPassword','','required');

			if ($this->form_validation->run()==FALSE)
			{
				return false;
			}
			else
			{
				return true;
			}
		}

		 	public  function situation($value)
	        {
	        	$input = $value;
	        	$marital = array('Already Sent','Already Called','Can`t Call','No Status');
	        	$digit = array(1,2,3,9);
	        	return str_replace($digit,$marital,$input);
	        }

		public function gender($value)
	        {
	        	$input = $value;
	        	$gender = array('Male','Female');
	        	$digit = array(1,2);
	        	return str_replace($digit,$gender,$input);
	        }

	    public function marital($value)
	        {
	        	$input = $value;
	        	$marital = array('Single','Married');
	        	$digit = array(1,2);
	        	return str_replace($digit,$marital,$input);
	        }

	    public function country($value)
	    {
	    	$this->db->where('t_id',$value);
	    	$val=$this->db->get('country_tbl')->row_array();
	    	return $val['d_name'];
	    }

		public function delivery_name($value){
			$this->db->where('t_id',$value);
			$val=$this->db->get('users_tbl')->row_array();
			if(count($val)==0){
			$val['first_name']='Walk in';
			$val['last_name']='Customer';
			}
			return $val['first_name'].' '.$val['last_name'];
		}

		public function loginState($username,$password)
		{
			$result='';
			$query = $this->db->query('SELECT * FROM users_tbl');
			if (!$query) 
			{
				die ('Error:'.mysql_error());
				
			}
				else 
				{
					foreach ($query->result() as $row)
					{
						if ($row->username == $username)
						{
							if ($row->password == md5($password))
							{
								$this->session->set_userdata('login_state','true');
								$this->session->set_userdata('username',$username);
								$this->session->set_userdata('user_role',$row->user_role);
								$this->session->set_userdata('user_id',$row->user_id);
								$this->session->set_userdata('real_name',$row->first_name.' '.$row->last_name);
								$result = 'true';
								break;
							}
							else 
							{
									$result = 'false';
							}
						}
							else {
								$result = 'false';
							}
							
					}

					if ($result == 'true')
					{
						return true;
					}
						else 
						{
							return false;
						}
				} 

		}

		public function insert_individual($val,$color,$product_id){
			if($color==''){
				$color='No Color';
			}
			$data =array(
				'color'=> $color,
				'quantity'=>$val,
				'product_id'=>$product_id
			);
			$this->db->insert('color_tbl',$data);

		}

		

		public function update_transfer_individual($val,$color,$product_id,$present){

			$this->db->where('color',$color);
			$this->db->where('product_id',$product_id);
			$p=$this->db->get('color_tbl')->row_array();

			if($val>$present){
				$val=$val-$present;
				$res=$p['quantity']-$val;
			}
			elseif($val<$present){
				$val=$present-$val;
				$res=$p['quantity']+$val;
			}
			elseif($val==$present){
				$res=$p['quantity'];
			}


			$this->db->where('color',$color);
			$this->db->where('product_id',$product_id);
			$this->db->update('color_tbl',array('quantity'=>$res));

		}

		public function update_individual($val,$color,$product_id){
			$data =array(
				'color'=> $color,
				'quantity'=>$val,
				'product_id'=>$product_id
			);
			$this->db->insert('color_tbl',$data);

		}


		public function delete_all($value)
		{
			$id=$value;
			$this->db->where('order_id',$id);
			$res=$this->db->get("order_items_tbl")->row_array();
			$q=$res['quantity'];
			$pid=$res['product_id'];
			$color=$res['color'];
			$this->delete_transfer($q,$pid,$color);
			$this->db->where('order_id',$id);
			$this->db->delete('order_items_tbl');
			$this->db->where('t_id',$id);
			$this->db->delete('order_list_tbl');

		}

		public function delete_transfer($q,$id,$color){

			$this->db->where('product_id',$id);
			$this->db->where('color',$color);
			$res=$this->db->get("color_tbl")->row_array();
			$total=$res['quantity']+$q;
			$this->db->where('product_id',$id);
			$this->db->where('color',$color);
			$this->db->update('color_tbl',array("quantity"=>$total));

		}

		public function getName($value,$type)
		{
		if($type=='img')
		{
			$this->db->where('t_id',$value);
			$val=$this->db->get('product_tbl')->row_array();
			if($val==true)
			{
				return $val['image_name'];
			}
			else{
				return "OTS";
			}
		}
			elseif ($type=='pname'){

				$value = explode(',',$value);
				if (count($value)>1){
					for ($x = 0; $x < count($value); $x++) {
						$this->db->where('t_id',$value[$x]);
						$val=$this->db->get('product_tbl')->row_array();
						$res[]=$val['name'];
					}
					$result = implode(', ',$res);
				}
				else{

					$this->db->where('t_id',$value[0]);
					$val=$this->db->get('product_tbl')->row_array();
					if(isset($val['name'])){
						$result=$val['name'];
					}else{
						$result='';
					}

				}

				if($val==true)
				{
					return $result;
				}
				else{
					return "Not Available";
				}

			}

			elseif ($type=='pcode') {

				$this->db->where('t_id',$value);
				$val=$this->db->get('product_tbl')->row_array();
				if($val==true)
				{
					return $val['item_code'];
				}
				else{
					return "Not Available";
				}

			}
			elseif ($type=='ptype'){

				$this->db->where('t_id',$value);
				$val=$this->db->get('product_tbl')->row_array();
				if($val==true)
				{
					return $this->p_type($val['p_type']);
				}
				else{
					return "Not Available";
				}
			}
		elseif ($type=='cate'){

			$this->db->where('t_id',$value);
			$val=$this->db->get('product_tbl')->row_array();
			if($val==true)
			{
				return $this->category($val['category']);
			}
			else{
				return "Not Available";
			}
		}
		elseif ($type=='price'){

			$this->db->where('t_id',$value);
			$val=$this->db->get('product_tbl')->row_array();
			if($val==true)
			{
				return $val['s_price'];
			}
			else{
				return "Not Available";
			}
		}
			

		}
		
		



		public function insert_transfer_individual($val,$color,$product_id){

			$this->db->where('color',$color);
			$this->db->where('product_id',$product_id);
			$p=$this->db->get('color_tbl')->row_array();
			$res=$p['quantity']-$val;
			$this->db->where('color',$color);
			$this->db->where('product_id',$product_id);
			$this->db->update('color_tbl',array('quantity'=>$res));

		}

		public function mutipleOrderInsert($product_id,$color,$quantity,$order_id){

		$res=$this->db->query("SELECT SUM(quantity) as quantity FROM color_tbl WHERE color = '$color' AND product_id = '$product_id'; ")->row_array();
		$res=$res['quantity'];
		if($res<$quantity){
			$res='error';
		}
		elseif($res>=$quantity) {
			$data = array(
				'order_id'=> $order_id,
				'product_id'=>$product_id,
				'color'=>$color,
				'quantity'=>$quantity
			);
			$this->db->insert('order_items_tbl', $data);
			$this->insert_transfer_individual($quantity, $color, $product_id);
			$res='ok';
		}

		return $res;
	}

		public function mutipleOrderEdit($product_id,$color,$quantity,$id){

			$res=$this->db->query("SELECT SUM(quantity) as quantity FROM color_tbl WHERE color = '$color' AND product_id = '$product_id'; ")->row_array();
			$res=$res['quantity'];
			if($res<$quantity){
				$res='error';
			}
			elseif($res>=$quantity) {
				$data = array(
					'product_id'=>$product_id,
					'color'=>$color,
					'quantity'=>$quantity
				);
				$this->db->where('id',$id);
				$last_q=$this->db->get('order_items_tbl')->row_array();

				if ($last_q['product_id']!=$product_id || $last_q['color']!=$color ){
					$callbid= $last_q['product_id'];
					$callbcolor= $last_q['color'];
					$callbqquan= $last_q['quantity'];
					$this->db->where('color',$callbcolor);
					$this->db->where('product_id',$callbid);
					$cbpresent=$this->db->get('color_tbl')->row_array();
					$res=$cbpresent['quantity']+$callbqquan;
					$this->db->where('color',$callbcolor);
					$this->db->where('product_id',$callbid);
					$this->db->update('color_tbl',array('quantity'=>$res));
					$this->insert_transfer_individual($quantity, $color, $product_id);
				}
				else{
					$this->edit_transfer_individual($quantity, $color, $product_id,$last_q['quantity']);
				}

				$this->db->where('id',$id);
				$this->db->update('order_items_tbl', $data);
				$res='ok';
			}

			return $res;
		}

		public function edit_transfer_individual($quantity,$color,$product_id,$last_q){

			$this->db->where('color',$color);
			$this->db->where('product_id',$product_id);
			$p=$this->db->get('color_tbl')->row_array();
			$result = $last_q - $quantity;
			if ($result<0){
				$val= abs($result);
			$res=$p['quantity']-$val;
			$this->db->where('color',$color);
			$this->db->where('product_id',$product_id);
			$this->db->update('color_tbl',array('quantity'=>$res));
				
			}
			elseif ($result>0){

				$val= $result;
				$res=$p['quantity']+$val;
				$this->db->where('color',$color);
				$this->db->where('product_id',$product_id);
				$this->db->update('color_tbl',array('quantity'=>$res));
			}
			elseif($result==0){

				$res=$p['quantity'];
				$this->db->where('color',$color);
				$this->db->where('product_id',$product_id);
				$this->db->update('color_tbl',array('quantity'=>$res));
			}
//
//


		}


		// ------------------------ Upload Image Function --------------------------

		public function upload_img($userfile)
		{
			$file = $_FILES[$userfile]['name'];
			$this->load->library('upload');
			$config['upload_path'] = 'upload/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = '5000';
			$config['max_width'] = '1024';
			$config['max_height'] = '768';
			$this->upload->initialize($config);
			if ($this->upload->do_upload())
			{
				return $file;
			}

		}

	function userCreate_formValidate()
	{
		$this->form_validation->set_rules('dep','','required');
		$this->form_validation->set_rules('position','','required');
		$this->form_validation->set_rules('name','','required');
		$this->form_validation->set_rules('username','','required');
		$this->form_validation->set_rules('password','','required');
		$this->form_validation->set_rules('rePassword','','required|matches[password]');
		$this->form_validation->set_rules('role','','required');

		if ($this->form_validation->run()==FALSE)
		{
			return false;
		}
		else
		{
			return true;
		}
	}





	}