<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends CI_Controller {

    function __construct() 
    {             
        parent::__construct();
 		
     		if ($this->session->userdata('login_state') != 'true')
     		{
     			redirect('site');
     		}
        $this->load->library('upload');

    }



    public function index()
    {

        $this->breadcrumbs->push('Home','admin');
        $data['loginame']=$this->session->userdata('real_name');
        $data['user_role']=$this->session->userdata('user_role');
        $data['pagetitle']="Product List";
        $data["sidebar_menu"]="admin/template/sidebar_menu";
        $data["query"]=$this->db->query('
                SELECT  pro.s_price as s_price,pro.price as price,pro.category as category,color.product_id as product_id,pro.p_type as p_type,GROUP_CONCAT(color SEPARATOR ", ") as color,SUM(color.quantity) as quantity,pro.image_name as image_name,
                pro.item_code as item_code,pro.name as name,pro.creat_date as creat_date,pro.t_id as t_id,pro.who_created as who_created FROM product_tbl as pro LEFT JOIN  color_tbl as color ON pro.t_id = color.product_id  GROUP BY color.product_id ORDER BY pro.creat_date DESC')->result_array();
        $data["trans"]=$this->db->query('SELECT  product_id as p_id,SUM(quantity) as quantity FROM order_items_tbl GROUP BY product_id ')->result();
        $data['main_content']='admin/product/product';
        $this->load->view('admin/template/admin_template',$data);
    }

    public function add_new_product()
    {

        $this->breadcrumbs->push('Home','admin');
        $this->breadcrumbs->push('Add New Product','admin/add-new-product');
        $data['loginame']=$this->session->userdata('real_name');
        $data['user_role']=$this->session->userdata('user_role');
        $data['pagetitle']="Customer Orders";
        $data["sidebar_menu"]="admin/template/sidebar_menu";
        $data["cat"]=$this->db->get('category_tbl')->result_array();
        $data['main_content']='admin/product/add_product_form';
        $this->load->view('admin/template/admin_template',$data);
    }

    public function add_product_process(){
        $p_type=$this->input->post('p_type');
        $product=$this->input->post('product');
        $category=$this->input->post('category');
        $item_code=$this->input->post('item_code');
        $color=$this->input->post('color');
        $price = $this->input->post('price');
        $selling_price = $this->input->post('s_price');
        $quantity=$this->input->post('quantity');
        $i=0;
        $file = $this->main_model->upload_img('userfile');
        date_default_timezone_set("Asia/Rangoon");
        $time =  strtotime(date("Y-m-d H:i:s"));
        $data=array(
            'p_type'=>$p_type,
            'name'=>$product,
            'category'=>$category,
            'item_code'=>$item_code,
            'image_name'=>$file,
            'price' => $price,
            's_price' => $selling_price,
            'creat_date'=>$time,
            'who_created'=>$this->session->userdata('real_name')
        );
        $this->db->insert('product_tbl',$data);
        $product_id = $this->db->select('t_id')->order_by('t_id','desc')->limit(1)->get('product_tbl')->row('t_id');
        foreach($quantity as $val)
        {
            $this->main_model->insert_individual($val,$color[$i++],$product_id);

        }
        redirect('product');
    }

    public function edit_product(){

        $id=$this->uri->segment(3);
        $this->breadcrumbs->push('Home','admin');
        $this->breadcrumbs->push('Edit Product','admin/edit-product');
        $data['loginame']=$this->session->userdata('real_name');
        $data['user_role']=$this->session->userdata('user_role');
        $data['pagetitle']="Edit Product";
        $data["sidebar_menu"]="admin/template/sidebar_menu";
        $this->db->where('t_id',$id);
        $data["row"]=$this->db->get('product_tbl')->row_array();
        $data["cat"]=$this->db->get('category_tbl')->result_array();
        //                $data["trans"]=$this->db->query("SELECT  color as color,product_id as p_id,SUM(quantity) as quantity FROM transfercolor_tbl WHERE product_id='$id' GROUP BY color")->result();
        $data['main_content']='admin/product/edit_product_form';
        $this->load->view('admin/template/admin_template',$data);

    }

    public  function edit_product_process(){
        $id=$this->uri->segment(3);
        $p_type=$this->input->post('p_type');
        $product=$this->input->post('product');
        $category=$this->input->post('category');
        $item_code=$this->input->post('item_code');
        $purchase_price = $this->input->post('purchase_price');
        $sell_price = $this->input->post('selling_price');
        $color=$this->input->post('color');
        $quantity=$this->input->post('quantity');
        $i=0;

        $file = $this->main_model->upload_img('userfile');

        if(empty($file)){
            $this->db->where("t_id",$id);
            $returnImage=$this->db->get("product_tbl")->row_array();
            $file=$returnImage["image_name"];
        }
        $data=array(
            'p_type'=>$p_type,
            'name'=>$product,
            'category'=>$category,
            'item_code'=>$item_code,
            'price' => $purchase_price,
            's_price' => $sell_price,
            'image_name'=>$file,
            'who_created'=>$this->session->userdata('real_name')
        );
        $this->db->where('t_id',$id);
        $this->db->update('product_tbl',$data);
        $product_id = $this->db->select('t_id')->where('t_id',$id)->get('product_tbl')->row('t_id');
        foreach($quantity as $val)
        {
            $this->main_model->update_individual($val,$color[$i++],$product_id);

        }
        redirect('product');
    }

    public function delete_product(){
        $id=$this->input->post('id');
        $this->db->where('t_id',$id);
        $this->db->delete('product_tbl');
        $this->db->where('product_id',$id);
        $this->db->delete('color_tbl');
    }

    public function update_color_q(){

        $idval=$this->input->post('idval');;
        $id2=$this->input->post('id2'); // ex
        $cname=$this->input->post('cname'); // ex
        $quantity=$this->input->post('quantity');
//                $this->db->where('t_id',$idval);
//                $q=$this->db->get('color_tbl')->row_array();
//                $present=$q['quantity'];
//                $t=$this->db->query("SELECT SUM(quantity) as quantity FROM transfercolor_tbl WHERE color = '$cname' AND product_id = '$id2'; ")->row_array();
//                $minus=$t['quantity'];
//                $res=$present - $minus; // remaining result 40
//                $quantity=abs($res)-abs($quantity); // res 40- q 30 = 10
//                if($quantity < 0){
//                    $quantity=$present-$quantity;
//                }
//                else {
//                    $quantity= $minus+$res-$quantity;
//                }
        // 10 + 5
        $data=array(
            'quantity'=>$quantity
        );
        $this->db->where('t_id',$idval);
        $this->db->update('color_tbl',$data);
        echo '<h5>Complete!</h5>';

    }

    public function delete_color(){
        $t_id=$this->input->post('t_id');
        $this->db->where('t_id',$t_id);
        $this->db->delete('color_tbl');

    }

    


    public function out_of_stock()
    {

        $this->breadcrumbs->push('Home','admin');
        $this->breadcrumbs->push('Out of stock list','admin/out-of-stock');
        $data['loginame']=$this->session->userdata('real_name');
        $data['user_role']=$this->session->userdata('user_role');
        $data['pagetitle']="Out of stock list";
        $data["sidebar_menu"]="admin/template/sidebar_menu";
        $data["query"]=$this->db->query('
                SELECT  pro.s_price as s_price,pro.price as price,pro.category as category,color.product_id as product_id,pro.p_type as p_type,GROUP_CONCAT(color SEPARATOR ", ") as color,SUM(color.quantity) as quantity,pro.image_name as image_name,
                pro.item_code as item_code,pro.name as name,pro.creat_date as creat_date,pro.t_id as t_id,pro.who_created as who_created FROM product_tbl as pro LEFT JOIN  color_tbl as color ON pro.t_id = color.product_id WHERE color.quantity = 0  GROUP BY color.product_id ORDER BY pro.creat_date DESC')->result_array();
        $data["trans"]=$this->db->query('SELECT  product_id as p_id,SUM(quantity) as quantity FROM order_items_tbl GROUP BY product_id ')->result();
//        $data["trans"]=$this->db->query('SELECT  product_id as p_id,SUM(quantity) as quantity FROM transfercolor_tbl GROUP BY product_id ')->result();
        $data['main_content']='admin/product/product';
        $this->load->view('admin/template/admin_template',$data);
    }
//
    public function below_stock()
    {

        $this->breadcrumbs->push('Home','admin');
        $this->breadcrumbs->push('Out of stock list','admin/out-of-stock');
        $data['loginame']=$this->session->userdata('real_name');
        $data['user_role']=$this->session->userdata('user_role');
        $data['pagetitle']="Below 5 Q";
        $data["sidebar_menu"]="admin/template/sidebar_menu";
        $data["query"]=$this->db->query('
                SELECT  pro.s_price as s_price,pro.price as price,pro.category as category,color.product_id as product_id,pro.p_type as p_type,GROUP_CONCAT(color.color SEPARATOR ", ") as color,SUM(color.quantity) as quantity,pro.image_name as image_name,
                pro.item_code as item_code,pro.name as name,pro.creat_date as creat_date,pro.t_id as t_id,pro.who_created as who_created FROM product_tbl as pro LEFT JOIN  color_tbl as color ON pro.t_id = color.product_id WHERE color.quantity < 5  GROUP BY color.product_id ORDER BY pro.creat_date DESC')->result_array();
        $data["trans"]=$this->db->query('SELECT  product_id as p_id,SUM(quantity) as quantity FROM order_items_tbl GROUP BY product_id ')->result();
        $data['main_content']='admin/product/product';
        $this->load->view('admin/template/admin_template',$data);
    }

    public function return_item()
    {
        $this->breadcrumbs->push('Home','admin');
        $this->breadcrumbs->push('Return','admin/return');
        $cid = $this->uri->segment(3);
        $parent = $this->uri->segment(4);
        $data['loginame']=$this->session->userdata('real_name');
        $data['user_role']=$this->session->userdata('user_role');
        $data['pagetitle']="Category Filter";
        $data["sidebar_menu"]="admin/template/sidebar_menu";
        $data["query"]=$this->db->query('
                SELECT  pro.s_price as s_price,pro.price as price,pro.category as category,color.product_id as product_id,pro.p_type as p_type,GROUP_CONCAT(color.color SEPARATOR ", ") as color,SUM(color.quantity) as quantity,pro.image_name as image_name,
                pro.item_code as item_code,pro.name as name,pro.creat_date as creat_date,pro.t_id as t_id,pro.who_created as who_created FROM product_tbl as pro LEFT JOIN  color_tbl as color ON pro.t_id = color.product_id WHERE pro.p_type = 2  GROUP BY color.product_id ORDER BY pro.creat_date DESC')->result_array();
        $data["trans"]=$this->db->query('SELECT  product_id as p_id,SUM(quantity) as quantity FROM order_items_tbl GROUP BY product_id ')->result();
        $data['main_content']='admin/product/product';
        $this->load->view('admin/template/admin_template',$data);
    }


    public function return_damage()
    {
        $this->breadcrumbs->push('Home','admin');
        $this->breadcrumbs->push('Return Damage','admin/return-damage');
        $cid = $this->uri->segment(3);
        $parent = $this->uri->segment(4);
        $data['loginame']=$this->session->userdata('real_name');
        $data['user_role']=$this->session->userdata('user_role');
        $data['pagetitle']="Category Filter";
        $data["sidebar_menu"]="admin/template/sidebar_menu";
        $data["query"]=$this->db->query('
                SELECT  pro.s_price as s_price,pro.price as price,pro.category as category,color.product_id as product_id,pro.p_type as p_type,GROUP_CONCAT(color.color SEPARATOR ", ") as color,SUM(color.quantity) as quantity,pro.image_name as image_name,
                pro.item_code as item_code,pro.name as name,pro.creat_date as creat_date,pro.t_id as t_id,pro.who_created as who_created FROM product_tbl as pro LEFT JOIN  color_tbl as color ON pro.t_id = color.product_id WHERE pro.p_type = 3  GROUP BY color.product_id ORDER BY pro.creat_date DESC')->result_array();
        $data["trans"]=$this->db->query('SELECT  product_id as p_id,SUM(quantity) as quantity FROM order_items_tbl GROUP BY product_id ')->result();
        $data['main_content']='admin/product/product';
        $this->load->view('admin/template/admin_template',$data);
    }


    public function return_repair()
    {
        $this->breadcrumbs->push('Home','admin');
        $this->breadcrumbs->push('Return Repair','admin/return-repair');
        $cid = $this->uri->segment(3);
        $parent = $this->uri->segment(4);
        $data['loginame']=$this->session->userdata('real_name');
        $data['user_role']=$this->session->userdata('user_role');
        $data['pagetitle']="Category Filter";
        $data["sidebar_menu"]="admin/template/sidebar_menu";
        $data["query"]=$this->db->query('
                SELECT  pro.s_price as s_price,pro.price as price,pro.category as category,color.product_id as product_id,pro.p_type as p_type,GROUP_CONCAT(color.color SEPARATOR ", ") as color,SUM(color.quantity) as quantity,pro.image_name as image_name,
                pro.item_code as item_code,pro.name as name,pro.creat_date as creat_date,pro.t_id as t_id,pro.who_created as who_created FROM product_tbl as pro LEFT JOIN  color_tbl as color ON pro.t_id = color.product_id WHERE pro.p_type = 4  GROUP BY color.product_id ORDER BY pro.creat_date DESC')->result_array();
        $data["trans"]=$this->db->query('SELECT  product_id as p_id,SUM(quantity) as quantity FROM order_items_tbl GROUP BY product_id ')->result();
        $data['main_content']='admin/product/product';
        $this->load->view('admin/template/admin_template',$data);
    }





}