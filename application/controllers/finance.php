<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Finance extends CI_Controller {

    private $data;

    function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('login_state') != 'true')
        {
            redirect('site');
        }
        $this->load->library('upload');

    }

    public function index()  {
        $this->breadcrumbs->push('Home','admin/index');
        $this->breadcrumbs->push('Finance','admin/finance');
        $data['loginame']=$this->session->userdata('real_name');
        $data['user_role']=$this->session->userdata('user_role');
        $data['pagetitle']="Customer Orders";
        $data["sidebar_menu"]="admin/template/sidebar_menu";
        $date= Date('Y-m-d');
        $data["query2"]=$this->db->query("SELECT `o`.*,SUM(o.total_amount) as total_amount,oi.product_id,GROUP_CONCAT(oi.product_id  SEPARATOR ',' ) as Pname,GROUP_CONCAT(oi.color  SEPARATOR ', ' ) as color,SUM(oi.quantity) as quantity FROM order_list_tbl as o LEFT JOIN order_items_tbl as  oi ON oi.order_id = o.t_id WHERE (o.disable_at = 3 OR o.situation = 0 )  AND DATE(FROM_UNIXTIME(o.voucher_date))= '$date' GROUP BY oi.product_id ORDER BY o.create_date DESC  ")->result_array();
        $data["query"]=$this->db->query("SELECT `o`.*,oi.product_id,GROUP_CONCAT(oi.product_id  SEPARATOR ',' ) as Pname,GROUP_CONCAT(oi.color  SEPARATOR ', ' ) as color,SUM(oi.quantity) as quantity FROM order_list_tbl as o LEFT JOIN order_items_tbl as  oi ON oi.order_id = o.t_id WHERE (o.disable_at = 3 OR o.situation = 0 )  AND DATE(FROM_UNIXTIME(o.voucher_date))= '$date' GROUP BY oi.order_id ORDER BY o.create_date DESC  ")->result_array();
       $data['main_content']='admin/finance/finance';
        $this->load->view('admin/admin_template',$data);
    }

    public function search(){
       $star_date=  $this->input->post('start_date');
        $end_date=  $this->input->post('end_date');

        $this->breadcrumbs->push('Home','admin/index');
        $this->breadcrumbs->push('Finance','admin/finance');
        $data['loginame']=$this->session->userdata('real_name');
        $data['user_role']=$this->session->userdata('user_role');
        $data['pagetitle']="Customer Orders";
        $data["sidebar_menu"]="admin/template/sidebar_menu";
        $date= Date('Y-m-d');
        $data["query"]=$this->db->query("SELECT `o`.*,oi.product_id,GROUP_CONCAT(oi.product_id  SEPARATOR ',' ) as Pname,GROUP_CONCAT(oi.color  SEPARATOR ', ' ) as color,SUM(oi.quantity) as quantity FROM order_list_tbl as o LEFT JOIN order_items_tbl as  oi ON oi.order_id = o.t_id WHERE (o.disable_at = 3 OR o.situation = 0 )  AND DATE(FROM_UNIXTIME(o.voucher_date))  BETWEEN '$star_date' AND '$end_date' GROUP BY oi.order_id ORDER BY o.create_date DESC  ")->result_array();
        $data["query2"]=$this->db->query("SELECT `o`.*,SUM(o.total_amount) as total_amount,oi.product_id,GROUP_CONCAT(oi.product_id  SEPARATOR ',' ) as Pname,GROUP_CONCAT(oi.color  SEPARATOR ', ' ) as color,SUM(oi.quantity) as quantity FROM order_list_tbl as o LEFT JOIN order_items_tbl as  oi ON oi.order_id = o.t_id WHERE (o.disable_at = 3 OR o.situation = 0 )  AND DATE(FROM_UNIXTIME(o.voucher_date))  BETWEEN '$star_date' AND '$end_date' GROUP BY oi.product_id ORDER BY o.create_date DESC  ")->result_array();
        $data['main_content']='admin/finance/finance';
        $this->load->view('admin/admin_template',$data);
    }



}