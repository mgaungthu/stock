<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transfer extends CI_Controller {

    private $data;

    function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('login_state') != 'true')
        {
            redirect('site');
        }
        $this->load->library('upload');

    }







    public function product_detail()
    {
        if($this->uri->segment(4)=='muti'){
            $data['uniqueid']= urldecode($this->uri->segment(5));
            $this->load->view('admin/transfer/muti_detail_list',$data);
        }
        else{
            $this->load->view('admin/transfer/detail_list');
        }

    }

    public function remaining_items(){
        $this->load->view('admin/transfer/remaining_items');
    }

    public  function mutiproduct(){
        $data['id'] =  $this->input->post('number');
        $this->load->view('admin/transfer/mutiproduct',$data);
    }

    public function add_transfer_process(){

        $first_name=$this->input->post("first_name");
        $last_name=$this->input->post("last_name");
        $ph_no=$this->input->post("ph_no");
        $address=$this->input->post("address");
        $note_pad=$this->input->post("note_pad");
        $city=$this->input->post("city");
        $delivery_name=$this->input->post('delivery_name');
        $product_id=$this->input->post('product_name');
        $color=$this->input->post('color');
        $quantity=$this->input->post('quantity');
        date_default_timezone_set("Asia/Rangoon");
        $time = strtotime(date("Y-m-d H:i:s"));
        $data = array(
            'first_name' => $first_name,
            'last_name' =>$last_name ,
            'ph_no' => $ph_no,
            'address' => $address,
            'note_pad' => $note_pad,
            'city' => $city,
            'delivery_name'=>$delivery_name,
            'create_date' => $time,
            'who_created' => $this->session->userdata('real_name')
        );
        $this->db->insert('order_list_tbl',$data);
        $order_id=$this->db->insert_id();
        for ($x = 0; $x < count($product_id); $x++) {

            $result = $this->main_model->mutipleOrderInsert($product_id[$x],$color[$x],$quantity[$x],$order_id);

        }

        redirect("admin/more-detail/".$order_id);

        


    }

    public function edit_transfer_process(){


        $id = $this->uri->segment(3);
        $first_name=$this->input->post("first_name");
        $last_name=$this->input->post("last_name");
        $ph_no=$this->input->post("ph_no");
        $address=$this->input->post("address");
        $note_pad=$this->input->post("note_pad");
        $city=$this->input->post("city");
        $delivery_name=$this->input->post('delivery_name');
        $product_id=$this->input->post('product_name');
        $color=$this->input->post('color');
        $quantity=$this->input->post('quantity');

        $data = array(
            'first_name' => $first_name,
            'last_name' =>$last_name ,
            'ph_no' => $ph_no,
            'address' => $address,
            'note_pad' => $note_pad,
            'city' => $city,
            'delivery_name'=>$delivery_name,

        );
        $this->db->where('t_id',$id);
         $this->db->update('order_list_tbl',$data);
        $order_id=$id;
        $this->db->where("order_id",$order_id);
        $q=$this->db->get("order_items_tbl")->result_array();

        foreach ($q as $result){

            $res[]= $result['id'];
        }


        for ($x = 0; $x < count($res); $x++) {


//        echo $color[$x].',';

        $this->main_model->mutipleOrderEdit($product_id[$x],$color[$x],$quantity[$x],$res[$x]);

        }

            for ($x = count($res); $x < count($product_id); $x++) {


//                echo $color[$x].',';

                $this->main_model->mutipleOrderInsert($product_id[$x],$color[$x],$quantity[$x],$order_id);

            }
        redirect("admin/more-detail/".$id);


    }

    public function edit_transfer(){
        $id=$this->uri->segment(3);
        $this->breadcrumbs->push('Home','admin');
        $this->breadcrumbs->push('Transfer List','transfer/transfer-list');
        $this->breadcrumbs->push('Edit Transfer','admin/add-new-product');
        $data['loginame']=$this->session->userdata('real_name');
        $data['user_role']=$this->session->userdata('user_role');
        $data['pagetitle']="Edit Transfer";
        $data["sidebar_menu"]="admin/template/sidebar_menu";
        $this->db->where('t_id',$id);
        $data["row"]=$this->db->get('transfer_tbl')->row_array();
        $data['main_content']='admin/transfer/edit_transfer_form';
        $this->load->view('admin/template/admin_template',$data);
    }

    public function delete_transfer(){
        $id=$this->input->post('id');
        $this->db->where('t_id',$id);
        $res=$this->db->get("transfer_tbl")->row_array();
        $q=$res['quantity'];
        $pid=$res['product_id'];
        $color=$res['color'];
        $this->main_model->delete_transfer($q,$pid,$color);
        $this->db->where('t_id',$id);
        $this->db->delete('transfer_tbl');
    }



}