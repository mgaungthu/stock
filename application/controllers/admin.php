<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

    function __construct() 
    {             
        parent::__construct();
 		
     		if ($this->session->userdata('login_state') != 'true')
     		{
     			redirect('site');
     		}
        $this->load->library('upload');

    }


//      public function hello()
//     {
//         $sql = "SET GLOBAL event_scheduler=\"ON\"";
//         $this->db->query($sql);
//         $this->breadcrumbs->push('Home','admin/index');
//         $data['loginame']=$this->session->userdata('real_name');
//         $data['user_role']=$this->session->userdata('user_role');
//         $data['pagetitle']="Customer Orders";
//         $data["sidebar_menu"]="admin/template/sidebar_menu";
//         $data["query"]=$this->db->query("SELECT `o`.*,o.t_id,o.first_name,o.last_name,o.ph_no,o.address,o.city,o.create_date,o.situation,oi.product_id,GROUP_CONCAT(oi.product_id  SEPARATOR ',' ) as Pname,GROUP_CONCAT(oi.color  SEPARATOR ', ' ) as color,SUM(oi.quantity) as quantity FROM order_list_tbl as o LEFT JOIN order_items_tbl as  oi ON oi.order_id = o.t_id WHERE o.disable_at = 1 GROUP BY oi.order_id ORDER BY o.create_date DESC  ")->result_array();
//         $data['main_content']='admin/home';
//         $this->load->view('admin/admin_template',$data);
//      }

    public function index(){

        $sql = "SET GLOBAL event_scheduler=\"ON\"";
        $this->db->query($sql);
        $this->breadcrumbs->push('Home','admin/index');
        $data['loginame']=$this->session->userdata('real_name');
        $data['user_role']=$this->session->userdata('user_role');
        $data['pagetitle']="Customer Orders";
        $data["sidebar_menu"]="admin/template/sidebar_menu";
        $data['main_content']='admin/home';
        $this->load->view('admin/admin_template',$data);
    }

    public function table(){

        $this->load->library('datatables');
        $this->datatables
            ->select("order_list_tbl.t_id as id,image_name,name,color,item_code,first_name as first_name, last_name , ph_no, order_list_tbl.city,FROM_UNIXTIME(order_list_tbl.create_date),situation " , FALSE)
            ->from('order_list_tbl')
            ->unset_column('situation')
            ->unset_column('t_id')
            ->unset_column('last_name')
            ->join('order_items_tbl', 'order_list_tbl.t_id=order_items_tbl.order_id', 'left')
            ->join('product_tbl', 'product_tbl.t_id=order_items_tbl.product_id', 'left')
            ->group_by("order_items_tbl.order_id ");

        $del='<input id="delAll" name="delValue" value="$1" onchange="valueChanged()" type="checkbox">';
        $image ="<img width=\"50\" src='upload/$1' class='img-responsive'/>";
        if($this->session->userdata('user_role')==1){
            $action = '<a data-toggle="tooltip" title="More Info" href="admin/voucher_detail/$1"><i class="fa fa-file-text-o"></i> </a> | <a data-toggle="tooltip" title="Edit"  href="admin/edit-order/$1"><i class="fa fa-pencil-square-o"></i> </a> | <a data-toggle="tooltip" title="Delete" href="admin/delete-order" id="$1" class="delete_order"><i class="fa fa-trash-o"></i> </a>';

        }
        else{
            $action = '<a data-toggle="tooltip" title="More Info" href="admin/voucher_detail/$1"><i class="fa fa-file-text-o"></i> </a>';

        }
        $situation = '<div id="showSelect-$2"> <div tid="$2" id="callStatusUpdate" val="$1" class="status color-$1">$3</div><span id="selectedbox"></span></div>';
        $this->datatables->edit_column("t_id",$del,'id');
        $this->datatables->edit_column("image_name",$image,'image_name');
        $this->datatables->edit_column("first_name",'$1 $2','first_name,last_name');
//        $this->datatables->edit_column("create_date",'$1','date_time(create_date)');
       $this->datatables->add_column("situation",$situation,'situation,id,situation(situation)');
       $this->datatables->add_column("action",$action,'id');
        echo $this->datatables->generate('json', 'ISO-8859-1');

    }

    public function callStatusUpdate(){
        $data['id']=$this->input->post('id');
        $data['value']=$this->input->post('value');
        $this->load->view('admin/callStatusUpdate',$data);

    }

    public function print_job($id){
        $data['sellPrice']=$this->input->post("sellPrice");
        $data['payamount']=$this->input->post("payamount");
        $data['discount']=$this->input->post("discount");
        $data['depdiscount']=$this->input->post("depdiscount");
        $paymethod=$this->input->post("paymethod");
        $data['deposit']=$this->input->post("deposit");
        $data['deposit2']=$this->input->post("deposit2");
        $data['paymethod']=$paymethod;
        $this->db->where('order_id',$id);
        $val=$this->db->get('order_items_tbl')->result_array();
        $this->db->where('t_id',$id);
        $res=$this->db->get('order_list_tbl')->row_array();
        if($res['voucher_no']==0000){
            $this->db->order_by('voucher_no','DESC');
            $all=$this->db->get('order_list_tbl')->row_array();
            $str = ltrim($all['voucher_no'], '0');
            $no = $str + 1;
            $this->db->where('t_id',$id);
            $this->db->update('order_list_tbl',array('situation'=>0,'voucher_no'=>$no));
            $this->db->where('t_id',$id);
            $res=$this->db->get('order_list_tbl')->row_array();
        }
        $this->db->where('t_id',$id);
        $this->db->update('order_list_tbl',array('situation'=>0));
        $data['id']=$id;
        $data['voucher_no']=$res['voucher_no'];
        $data['res']=$val;
        $this->load->view("admin/print/print",$data);
    }
      
      
      public function more_detail() {
         $this->breadcrumbs->push('Home','admin/index');
          $this->breadcrumbs->push('More Detail','#');
         $data['loginame']=$this->session->userdata('real_name');
         $data['user_role']=$this->session->userdata('user_role');
         $data['pagetitle']="More Detail";
         $data["sidebar_menu"]="admin/template/sidebar_menu";
        $ids = $this->uri->segment(3);
         $data['order_id']=$ids;
         $this->db->where("t_id",$ids);
          $data["row"]=$this->db->get('order_list_tbl')->row_array();
     	   $data['main_content']='admin/transfer/detail_transfer';
         $this->load->view('admin/admin_template',$data);  	
   	}

    public function voucher_detail() {
        $this->breadcrumbs->push('Home','admin/index');
        $this->breadcrumbs->push('More Detail','#');
        $data['loginame']=$this->session->userdata('real_name');
        $data['user_role']=$this->session->userdata('user_role');
        $data['pagetitle']="More Detail";
        $data["sidebar_menu"]="admin/template/sidebar_menu";
        $ids = $this->uri->segment(3);
        $data['order_id']=$ids;
        $this->db->where("t_id",$ids);
        $data["row"]=$this->db->get('order_list_tbl')->row_array();
        $data['main_content']='admin/transfer/detail_transfer_voucher';
        $this->load->view('admin/admin_template',$data);
    }

    public function dashboard()
    {

        $this->breadcrumbs->push('Home','admin/index');
        $data['loginame']=$this->session->userdata('real_name');
        $data['user_role']=$this->session->userdata('user_role');
        $data['pagetitle']="Customer Orders";
        $data["sidebar_menu"]="admin/template/sidebar_menu";
        $data['main_content']='admin/dashboard';
        $this->load->view('admin/admin_template',$data);
    }

    public function category_filter()
    {
        $this->breadcrumbs->push('Home','admin');
        $this->breadcrumbs->push('Category List','admin/categories');
        $this->breadcrumbs->push('Category Filter','admin/categoriey-filter');
        $cid = $this->uri->segment(3);
        $parent = $this->uri->segment(4);
        $data['loginame']=$this->session->userdata('real_name');
        $data['user_role']=$this->session->userdata('user_role');
        $data['pagetitle']="Category Filter";
        $data["sidebar_menu"]="admin/template/sidebar_menu";
        $data["query"]=$this->db->query('
                SELECT  pro.s_price as s_price,pro.price as price,pro.category as category,color.product_id as product_id,pro.p_type as p_type,GROUP_CONCAT(color) as color,SUM(color.quantity) as quantity,pro.image_name as image_name,
                pro.item_code as item_code,pro.name as name,pro.creat_date as creat_date,pro.t_id as t_id,pro.who_created as who_created FROM product_tbl as pro LEFT JOIN  color_tbl as color ON pro.t_id = color.product_id WHERE category = '.$cid.' GROUP BY color.product_id ORDER BY pro.creat_date DESC')->result_array();
        $data["trans"]=$this->db->query('SELECT  product_id as p_id,SUM(quantity) as quantity FROM order_items_tbl GROUP BY product_id ')->result();
        $data['main_content']='admin/product/product';
        $this->load->view('admin/template/admin_template',$data);
    }



    public function already_sent(){
        $this->breadcrumbs->push('Home','admin/index');
         $this->breadcrumbs->push('Already Sent','admin/already-sent');
         $data['loginame']=$this->session->userdata('real_name');
         $data['user_role']=$this->session->userdata('user_role');
         $data['pagetitle']="Customer Orders";
         $data["sidebar_menu"]="admin/template/sidebar_menu";
         //$data["query"]=$this->db->query("SELECT `o`.*,o.t_id,o.first_name,o.last_name,o.ph_no,o.address,o.city,o.create_date,o.situation,oi.product_id,GROUP_CONCAT(oi.product_id  SEPARATOR ',' ) as Pname,GROUP_CONCAT(oi.color  SEPARATOR ', ' ) as color,SUM(oi.quantity) as quantity FROM order_list_tbl as o LEFT JOIN order_items_tbl as  oi ON oi.order_id = o.t_id WHERE  o.situation = 1 GROUP BY oi.order_id ORDER BY o.create_date DESC  ")->result_array();
         $data['main_content']='admin/home';
         $this->load->view('admin/admin_template',$data);
      }

     public  function  get_already_sent(){
         $this->load->library('datatables');
         $this->datatables
             ->select("order_list_tbl.t_id as id,image_name,name,color,item_code,first_name as first_name, last_name , ph_no,  order_list_tbl.city,FROM_UNIXTIME(order_list_tbl.create_date),situation " , FALSE)
             ->from('order_list_tbl')
             ->unset_column('situation')
             ->unset_column('t_id')
             ->unset_column('last_name')
             ->join('order_items_tbl', 'order_list_tbl.t_id=order_items_tbl.order_id', 'left')
             ->join('product_tbl', 'product_tbl.t_id=order_items_tbl.product_id', 'left')
             ->where('situation',1)
             ->group_by("order_items_tbl.order_id ");

         $del='<input id="delAll" name="delValue" value="$1" onchange="valueChanged()" type="checkbox">';
         $image ="<img width=\"50\" src='upload/$1' class='img-responsive'/>";
         $action = '<a data-toggle="tooltip" title="More Info" href="admin/voucher_detail/$1"><i class="fa fa-file-text-o"></i> </a> | <a data-toggle="tooltip" title="Edit"  href="admin/edit-order/$1"><i class="fa fa-pencil-square-o"></i> </a> | <a data-toggle="tooltip" title="Delete" href="admin/delete-order" id="$1" class="delete_order"><i class="fa fa-trash-o"></i> </a>';
         $situation = '<div id="showSelect-$2"> <div tid="$2" id="callStatusUpdate" val="$1" class="status color-$1">$3</div><span id="selectedbox"></span></div>';
         $this->datatables->edit_column("t_id",$del,'id');
         $this->datatables->edit_column("image_name",$image,'image_name');
         $this->datatables->edit_column("first_name",'$1 $2','first_name,last_name');
         //$this->datatables->edit_column("create_date",'$1','date_time(create_date)');
         $this->datatables->add_column("situation",$situation,'situation,id,situation(situation)');
         $this->datatables->add_column("action",$action,'id');
         echo $this->datatables->generate('json', 'ISO-8859-1');
     }


        public function already_called()  {
        $this->breadcrumbs->push('Home','admin/index');
         $this->breadcrumbs->push('Already Called','admin/already-called');
         $data['loginame']=$this->session->userdata('real_name');
         $data['user_role']=$this->session->userdata('user_role');
         $data['pagetitle']="Customer Orders";
         $data["sidebar_menu"]="admin/template/sidebar_menu";
         //$data["query"]=$this->db->query("SELECT `o`.*,o.t_id,o.first_name,o.last_name,o.ph_no,o.address,o.city,o.create_date,o.situation,oi.product_id,GROUP_CONCAT(oi.product_id  SEPARATOR ',' ) as Pname,GROUP_CONCAT(oi.color  SEPARATOR ', ' ) as color,SUM(oi.quantity) as quantity FROM order_list_tbl as o LEFT JOIN order_items_tbl as  oi ON oi.order_id = o.t_id WHERE o.disable_at = 1 AND o.situation = 2 GROUP BY oi.order_id ORDER BY o.create_date DESC  ")->result_array();
        $data['main_content']='admin/home';
         $this->load->view('admin/admin_template',$data);
      }

    public  function  get_already_called(){
        $this->load->library('datatables');
        $this->datatables
            ->select("order_list_tbl.t_id as id,image_name,name,color,item_code,first_name as first_name, last_name , ph_no, order_list_tbl.city,FROM_UNIXTIME(order_list_tbl.create_date),situation " , FALSE)
            ->from('order_list_tbl')
            ->unset_column('situation')
            ->unset_column('t_id')
            ->unset_column('last_name')
            ->join('order_items_tbl', 'order_list_tbl.t_id=order_items_tbl.order_id', 'left')
            ->join('product_tbl', 'product_tbl.t_id=order_items_tbl.product_id', 'left')
            ->where('disable_at',1)
            ->where('situation',2)
            ->group_by("order_items_tbl.order_id ");

        $del='<input id="delAll" name="delValue" value="$1" onchange="valueChanged()" type="checkbox">';
        $image ="<img width=\"50\" src='upload/$1' class='img-responsive'/>";
        $action = '<a data-toggle="tooltip" title="More Info" href="admin/voucher_detail/$1"><i class="fa fa-file-text-o"></i> </a> | <a data-toggle="tooltip" title="Edit"  href="admin/edit-order/$1"><i class="fa fa-pencil-square-o"></i> </a> | <a data-toggle="tooltip" title="Delete" href="admin/delete-order" id="$1" class="delete_order"><i class="fa fa-trash-o"></i> </a>';
        $situation = '<div id="showSelect-$2"> <div tid="$2" id="callStatusUpdate" val="$1" class="status color-$1">$3</div><span id="selectedbox"></span></div>';
        $this->datatables->edit_column("t_id",$del,'id');
        $this->datatables->edit_column("image_name",$image,'image_name');
        $this->datatables->edit_column("first_name",'$1 $2','first_name,last_name');
       // $this->datatables->edit_column("create_date",'$1','date_time(create_date)');
        $this->datatables->add_column("situation",$situation,'situation,id,situation(situation)');
        $this->datatables->add_column("action",$action,'id');
        echo $this->datatables->generate('json', 'ISO-8859-1');
    }


      public function cant_called()  {
        $this->breadcrumbs->push('Home','admin/index');
         $this->breadcrumbs->push('Can`t Called','admin/cant-called');
         $data['loginame']=$this->session->userdata('real_name');
         $data['user_role']=$this->session->userdata('user_role');
         $data['pagetitle']="Customer Orders";
         $data["sidebar_menu"]="admin/template/sidebar_menu";
        //  $data["query"]=$this->db->query("SELECT `o`.*,o.t_id,o.first_name,o.last_name,o.ph_no,o.address,o.city,o.create_date,o.situation,oi.product_id,GROUP_CONCAT(oi.product_id  SEPARATOR ',' ) as Pname,GROUP_CONCAT(oi.color  SEPARATOR ', ' ) as color,SUM(oi.quantity) as quantity FROM order_list_tbl as o LEFT JOIN order_items_tbl as  oi ON oi.order_id = o.t_id WHERE o.disable_at = 1 AND o.situation = 3 GROUP BY oi.order_id ORDER BY o.create_date DESC  ")->result_array();

          $data['main_content']='admin/home';
         $this->load->view('admin/admin_template',$data);
      }

    public  function  get_cant_called(){
        $this->load->library('datatables');
        $this->datatables
            ->select("order_list_tbl.t_id as id,image_name,name,color,item_code,first_name as first_name, last_name , ph_no, order_list_tbl.city,FROM_UNIXTIME(order_list_tbl.create_date),situation " , FALSE)
            ->from('order_list_tbl')
            ->unset_column('situation')
            ->unset_column('t_id')
            ->unset_column('last_name')
            ->join('order_items_tbl', 'order_list_tbl.t_id=order_items_tbl.order_id', 'left')
            ->join('product_tbl', 'product_tbl.t_id=order_items_tbl.product_id', 'left')
            ->where('disable_at',1)
            ->where('situation',3)
            ->group_by("order_items_tbl.order_id ");

        $del='<input id="delAll" name="delValue" value="$1" onchange="valueChanged()" type="checkbox">';
        $image ="<img width=\"50\" src='upload/$1' class='img-responsive'/>";
        $action = '<a data-toggle="tooltip" title="More Info" href="admin/voucher_detail/$1"><i class="fa fa-file-text-o"></i> </a> | <a data-toggle="tooltip" title="Edit"  href="admin/edit-order/$1"><i class="fa fa-pencil-square-o"></i> </a> | <a data-toggle="tooltip" title="Delete" href="admin/delete-order" id="$1" class="delete_order"><i class="fa fa-trash-o"></i> </a>';
        $situation = '<div id="showSelect-$2"> <div tid="$2" id="callStatusUpdate" val="$1" class="status color-$1">$3</div><span id="selectedbox"></span></div>';
        $this->datatables->edit_column("t_id",$del,'id');
        $this->datatables->edit_column("image_name",$image,'image_name');
        $this->datatables->edit_column("first_name",'$1 $2','first_name,last_name');
        //$this->datatables->edit_column("create_date",'$1','date_time(create_date)');
        $this->datatables->add_column("situation",$situation,'situation,id,situation(situation)');
        $this->datatables->add_column("action",$action,'id');
        echo $this->datatables->generate('json', 'ISO-8859-1');
    }



      public function cancel()  {
        $this->breadcrumbs->push('Home','admin/index');
         $this->breadcrumbs->push('Canceled','admin/cancel');
         $data['loginame']=$this->session->userdata('real_name');
         $data['user_role']=$this->session->userdata('user_role');
         $data['pagetitle']="Customer Orders";
         $data["sidebar_menu"]="admin/template/sidebar_menu";
         //$this->db->order_by("create_date","DESC");
         //$this->db->where('situation',4);
         //$data["query"]=$this->db->get("order_list_tbl")->result_array();
         $data['main_content']='admin/home';
         $this->load->view('admin/admin_template',$data);
      }


    public  function  get_cancel(){
        $this->load->library('datatables');
        $this->datatables
            ->select("order_list_tbl.t_id as id,image_name,name,color,item_code,first_name as first_name, last_name , ph_no, order_list_tbl.city,FROM_UNIXTIME(order_list_tbl.create_date),situation " , FALSE)
            ->from('order_list_tbl')
            ->unset_column('situation')
            ->unset_column('t_id')
            ->unset_column('last_name')
            ->join('order_items_tbl', 'order_list_tbl.t_id=order_items_tbl.order_id', 'left')
            ->join('product_tbl', 'product_tbl.t_id=order_items_tbl.product_id', 'left')
            ->where('situation',4)
            ->group_by("order_items_tbl.order_id ");

        $del='<input id="delAll" name="delValue" value="$1" onchange="valueChanged()" type="checkbox">';
        $image ="<img width=\"50\" src='upload/$1' class='img-responsive'/>";
        $action = '<a data-toggle="tooltip" title="More Info" href="admin/voucher_detail/$1"><i class="fa fa-file-text-o"></i> </a> | <a data-toggle="tooltip" title="Edit"  href="admin/edit-order/$1"><i class="fa fa-pencil-square-o"></i> </a> | <a data-toggle="tooltip" title="Delete" href="admin/delete-order" id="$1" class="delete_order"><i class="fa fa-trash-o"></i> </a>';
        $situation = '<div id="showSelect-$2"> <div tid="$2" id="callStatusUpdate" val="$1" class="status color-$1">$3</div><span id="selectedbox"></span></div>';
        $this->datatables->edit_column("t_id",$del,'id');
        $this->datatables->edit_column("image_name",$image,'image_name');
        $this->datatables->edit_column("first_name",'$1 $2','first_name,last_name');
        //$this->datatables->edit_column("create_date",'$1','date_time(create_date)');
        $this->datatables->add_column("situation",$situation,'situation,id,situation(situation)');
        $this->datatables->add_column("action",$action,'id');
        echo $this->datatables->generate('json', 'ISO-8859-1');
    }


    public function voucher(){
                $this->breadcrumbs->push('Home','admin/index');
         $this->breadcrumbs->push('Voucher','admin/voucher');
         $data['loginame']=$this->session->userdata('real_name');
         $data['user_role']=$this->session->userdata('user_role');
         $data['pagetitle']="Customer Orders";
         $data["sidebar_menu"]="admin/template/sidebar_menu";
         $date= Date('Y-m-d');
         $data["query"]=$this->db->query("SELECT `o`.*,o.voucher_no,o.t_id,o.first_name,o.last_name,o.ph_no,o.address,o.city,o.create_date,o.situation,oi.product_id,GROUP_CONCAT(oi.product_id  SEPARATOR ',' ) as Pname,GROUP_CONCAT(oi.color  SEPARATOR ', ' ) as color,SUM(oi.quantity) as quantity FROM order_list_tbl as o LEFT JOIN order_items_tbl as  oi ON oi.order_id = o.t_id WHERE (o.disable_at = 3 OR o.situation = 0 )  AND DATE(FROM_UNIXTIME(o.voucher_date))= '$date' GROUP BY oi.order_id ORDER BY o.create_date DESC  ")->result_array();
         $data['main_content']='admin/voucher';
         $this->load->view('admin/admin_template',$data);
    }



      public function get_voucher()  {

          $date= strtotime(Date('Y-m-d'));  
//          $this->load->library('datatables');
//          $this->datatables
//              ->select("order_list_tbl.t_id as id,image_name,voucher_no,color,item_code,first_name as first_name, last_name , ph_no, order_list_tbl.city,order_items_tbl.quantity,total_amount,order_list_tbl.voucher_date,situation " , FALSE)
//              ->from('order_list_tbl')
//              ->unset_column('situation')
//              ->unset_column('t_id')
//              ->unset_column('last_name')
//              ->join('order_items_tbl', 'order_list_tbl.t_id=order_items_tbl.order_id  ', 'left')
//              ->join('product_tbl', 'product_tbl.t_id=order_items_tbl.product_id', 'left')
//              ->or_where('situation',0)
//              ->or_where('disable_at',3)
//              ->where('vouchewr_date',$date)
//              ->group_by("order_items_tbl.order_id ");
//
//          $del='<input id="delAll" name="delValue" value="$1" onchange="valueChanged()" type="checkbox">';
//          $image ="<img width=\"50\" src='upload/$1' class='img-responsive'/>";
//          $action = '<a data-toggle="tooltip" title="More Info" href="admin/more_detail/$1"><i class="fa fa-file-text-o"></i> </a> | <a data-toggle="tooltip" title="Edit"  href="admin/edit-order/$1"><i class="fa fa-pencil-square-o"></i> </a> | <a data-toggle="tooltip" title="Delete" href="admin/delete-order" id="$1" class="delete_order"><i class="fa fa-trash-o"></i> </a>';
//          $situation = '<div id="showSelect-$2"> <div tid="$2" id="callStatusUpdate" val="$1" class="status color-$1">$3</div><span id="selectedbox"></span></div>';
//          $this->datatables->edit_column("t_id",$del,'id');
//          $this->datatables->edit_column("image_name",$image,'image_name');
//          $this->datatables->edit_column("first_name",'$1 $2','first_name,last_name');
//          $this->datatables->edit_column("voucher_date",'$1','date_time(voucher_date)');
//          $this->datatables->add_column("situation",$situation,'situation,id,situation(situation)');
//          $this->datatables->add_column("action",$action,'id');
//          echo $this->datatables->generate('json', 'ISO-8859-1');
      }

    public function voucher_search()  {

        if($this->session->userdata('user_role')==1){


        $star_date=  $this->input->post('start_date');
        $end_date=  $this->input->post('end_date');
        $this->breadcrumbs->push('Home','admin/index');
        $this->breadcrumbs->push('Voucher','admin/voucher');
        $data['loginame']=$this->session->userdata('real_name');
        $data['user_role']=$this->session->userdata('user_role');
        $data['pagetitle']="Customer Orders";
        $data["sidebar_menu"]="admin/template/sidebar_menu";
        $data["query"]=$this->db->query("SELECT `o`.*,o.voucher_no,o.t_id,o.first_name,o.last_name,o.ph_no,o.address,o.city,o.create_date,o.situation,oi.product_id,GROUP_CONCAT(oi.product_id  SEPARATOR ',' ) as Pname,GROUP_CONCAT(oi.color  SEPARATOR ', ' ) as color,SUM(oi.quantity) as quantity FROM order_list_tbl as o LEFT JOIN order_items_tbl as  oi ON oi.order_id = o.t_id WHERE (o.disable_at = 3 OR o.situation = 0 ) AND DATE(FROM_UNIXTIME(o.voucher_date))  BETWEEN '$star_date' AND '$end_date' GROUP BY oi.order_id ORDER BY o.create_date DESC  ")->result_array();

        $data['main_content']='admin/voucher';
        $this->load->view('admin/admin_template',$data);

        }
    }

    

      public function add_order()
     {
         $this->breadcrumbs->push('Home','admin/index');
         $this->breadcrumbs->push('Add New Order','admin/add-order');
         $data['loginame']=$this->session->userdata('real_name');
         $data['pagetitle']="Add New Orders";
         $data["sidebar_menu"]="admin/template/sidebar_menu";
         $data['main_content']='admin/add_order_form';
         $this->load->view('admin/admin_template',$data);
      }

      public function edit_order()
     {
         $this->breadcrumbs->push('Home','admin/index');
         $this->breadcrumbs->push('Add New Order','admin/add-order');
         $t_id=$this->uri->segment(3);
         $this->db->where("t_id",$t_id);
         $data["row"]=$this->db->get("order_list_tbl")->row_array();
         $data['loginame']=$this->session->userdata('real_name');
         $data['pagetitle']="Add New Orders";
         $data["sidebar_menu"]="admin/template/sidebar_menu";
         $data['main_content']='admin/edit_order_form';
         $this->load->view('admin/admin_template',$data);
      }

      public function add_order_process()
      {
        date_default_timezone_set("Asia/Rangoon");
        $create_date =  strtotime(date("Y-m-d H:i:s"));
        $first_name=$this->input->post("first_name");
        $last_name=$this->input->post("last_name");
        $ph_no=$this->input->post("ph_no");
        $address=$this->input->post("address");
        $note_pad=$this->input->post("note_pad");
        $city=$this->input->post("city");
        $item_code  =$this->input->post("item_code");
        $quantity  =$this->input->post("quantity");
        $color  =$this->input->post("color");
        
        $data = array(
          'first_name' => $first_name, 
          'last_name' =>$last_name , 
          'ph_no' => $ph_no, 
          'address' => $address, 
          'note_pad' => $note_pad, 
          'city' => $city, 
          'item_code' => $item_code, 
          'quantity' => $quantity, 
          'color' => $color, 
          'create_date' => $create_date,
          'who_created' =>$this->session->userdata('real_name')
          );
        $this->db->insert("order_list_tbl",$data);
        redirect("admin");

      }

    public function edit_order_process()
      {

        $t_id=$this->uri->segment(3);
        $first_name=$this->input->post("first_name");
        $last_name=$this->input->post("last_name");
        $ph_no=$this->input->post("ph_no");
        $address=$this->input->post("address");
        $note_pad=$this->input->post("note_pad");
        $city=$this->input->post("city");
        $item_code  =$this->input->post("item_code");
        $quantity  =$this->input->post("quantity");
        $color  =$this->input->post("color");
        
        $data = array(
          'first_name' => $first_name, 
          'last_name' =>$last_name , 
          'ph_no' => $ph_no, 
          'address' => $address, 
          'note_pad'=>$note_pad,
          'city' => $city, 
          'item_code' => $item_code, 
          'quantity' => $quantity, 
          'color' => $color, 
          
          );
        $this->db->where("t_id",$t_id);
        $this->db->update("order_list_tbl",$data);
        redirect("admin");

      }

      public function delete_order()
      {
          $quantity='';
          $id=$this->input->post('id');
          $this->db->where('order_id',$id);
          $res=$this->db->get("order_items_tbl")->result_array();
          foreach ($res as $value){
              $quantity[]=$value['quantity'];
              $color[]=$value['color'];
              $pid[]=$value['product_id'];
          }

          for ($i=0; $i < count($pid); $i++) {

              $this->main_model->delete_transfer($quantity[$i], $pid[$i], $color[$i]);
          }
         
          $this->db->where('t_id',$id);
          $this->db->delete('order_list_tbl');
          $this->db->where('order_id',$id);
          $this->db->delete('order_items_tbl');
      }

    public function delete_order_product(){

        $id=$this->input->post('t_id');
        $this->db->where('id',$id);
        $pres=$this->db->get('order_items_tbl')->row_array();
       $quan  = $pres['quantity'];
       $color =  $pres['color'];
        $productid=$pres['product_id'];
        $this->db->where('color',$color);
        $this->db->where('product_id',$productid);
        $res=$this->db->get('color_tbl')->row_array();
        $total = $res['quantity']+$quan;
        $this->db->where('color',$color);
        $this->db->where('product_id',$productid);
        $this->db->update('color_tbl',array('quantity'=>$total));
        $this->db->where('id',$id);
        $this->db->delete('order_items_tbl');


    }

         public function situation_update()
     {  

          $user_role=$this->session->userdata('user_role');
          $t_id=$this->input->post("t_id");
          $sit=$this->input->post("sit");
          $this->db->where("t_id",$t_id);
          $row=$this->db->get("order_list_tbl")->row_array();
          if ($user_role==1) 
          {
              if ($sit==4)
              {
                  $this->db->where('order_id',$t_id);
                  $res=$this->db->get("order_items_tbl")->result_array();
                  foreach ($res as $value){
                      $quantity[]=$value['quantity'];
                      $color[]=$value['color'];
                      $pid[]=$value['product_id'];
                  }

                  for ($i=0; $i < count($pid); $i++) {

                      $this->main_model->delete_transfer($quantity[$i], $pid[$i], $color[$i]);
                  }
                  $data = array(
                      'situation' => $sit ,
                  );
                  $this->db->where("t_id",$t_id);
                  $this->db->update("order_list_tbl",$data);

                  $this->db->where('order_id',$t_id);
                  $this->db->delete('order_items_tbl');
              }
              else{
                  $data = array(
                      'situation' => $sit ,
                  );
                  $this->db->where("t_id",$t_id);
                  $this->db->update("order_list_tbl",$data);
              }

          }
          else{
              if ($row["situation"]==1) {
                $data = array(
                'pending' => "P ->" ,
                'pending_sit'=> $this->main_model->situation($sit) 
                      );            
                $update = array(
                'situation' => 4 , 
                'pending_sit'=> $sit
                ); 
                $this->db->where("t_id",$t_id);
                $this->db->update("order_list_tbl",$update);
                echo json_encode($data); 
              }
              else{
                  if ($sit==4)
                  {
                      $this->db->where('order_id',$t_id);
                      $res=$this->db->get("order_items_tbl")->result_array();
                      foreach ($res as $value){
                          $quantity[]=$value['quantity'];
                          $color[]=$value['color'];
                          $pid[]=$value['product_id'];
                      }


                      for ($i=0; $i < count($pid); $i++) {

                          $this->main_model->delete_transfer($quantity[$i], $pid[$i], $color[$i]);
                      }
                      $data = array(
                          'situation' => $sit ,
                      );
                      $this->db->where("t_id",$t_id);
                      $this->db->update("order_list_tbl",$data);

                      $this->db->where('order_id',$t_id);
                      $this->db->delete('order_items_tbl');
                  }
                  else{
                      $data = array(
                          'situation' => $sit ,
                      );
                      $this->db->where("t_id",$t_id);
                      $this->db->update("order_list_tbl",$data);

                  }

              }
        }
     }

      public function confirm_sit()
      {
        $data['change']=$this->input->post("change");
        $data['pending']=$this->input->post("pending");
        $this->load->view("admin/confirm_sit",$data);
      }



    public function categories()
    {
        $this->breadcrumbs->push('Home','admin');
        $this->breadcrumbs->push('Category List','admin/categorie');
        $data['loginame']=$this->session->userdata('real_name');
        $data['user_role']=$this->session->userdata('user_role');
        $data['pagetitle']="Category List";
        $data["sidebar_menu"]="admin/template/sidebar_menu";
        $data["count"]=$this->db->query('
                SELECT category,t_id,count(t_id) as quantity FROM product_tbl GROUP BY category ORDER BY creat_date DESC')->result_array();
        $data["query"]=$this->db->get('category_tbl')->result_array();
        $data['main_content']='admin/setting/category';
        $this->load->view('admin/template/admin_template',$data);
    }

    public function add_new_category()
    {

        $this->breadcrumbs->push('Home','admin');
        $this->breadcrumbs->push('Add New Category','admin/add-new-category');
        $data['loginame']=$this->session->userdata('real_name');
        $data['user_role']=$this->session->userdata('user_role');
        $data['pagetitle']="Customer Orders";
        $data["sidebar_menu"]="admin/template/sidebar_menu";
        //$data["cat"]=$this->main_model->fetchCategoryTree();
        $data['main_content']='admin/setting/new_category_form';
        $this->load->view('admin/template/admin_template',$data);
    }

    public function edit_category()
    {
        $id=$this->uri->segment(3);
        $this->breadcrumbs->push('Home','admin');
        $this->breadcrumbs->push('Add New Category','admin/add-new-category');
        $data['loginame']=$this->session->userdata('real_name');
        $data['user_role']=$this->session->userdata('user_role');
        $data['pagetitle']="Customer Orders";
        $data["sidebar_menu"]="admin/template/sidebar_menu";
        $this->db->where('cid',$id);
        $data['row']=$this->db->get('category_tbl')->row_array();
        $data['main_content']='admin/setting/new_category_form';
        $this->load->view('admin/template/admin_template',$data);
    }

    public function add_category_proccess(){
        $category= $this->input->post('category');
        $this->db->where('name',$category);
        $match=$this->db->get('category_tbl')->row_array();
        if($match==true){
            redirect('admin/add-new-category');
        }

        $data=  array(
            'name'=>$category,
        );
        $this->db->insert('category_tbl',$data);
        redirect('admin/categories');
    }

    public function edit_category_proccess(){
        $id=$this->uri->segment(3);
        $category= $this->input->post('category');

        $data=  array(
            'name'=>$category,


        );
        $this->db->where('cid',$id);
        $this->db->update('category_tbl',$data);
        redirect('admin/categories');
    }

    public function delete_cat(){
        $id=$this->input->post('id');
        $this->db->where('cid',$id);
        $this->db->delete('category_tbl');
    }

    public function user_management()
    {
        $this->breadcrumbs->push('Home','admin');
        $this->breadcrumbs->push('User Managment','admin/user-management');
        $this->db->order_by('user_role','ASC');
        $data['query']=$this->db->get('users_tbl')->result_array();
        $data["sidebar_menu"]="admin/template/sidebar_menu";
        $data['pagetitle']="User Managment";
        $data['loginame']=$this->session->userdata('real_name');
        $data['main_content']='admin/user_show';
        $this->load->view('admin/template/admin_template',$data);
    }

    public function user_detail()
    {
        $this->breadcrumbs->push('Home','admin');
        $this->breadcrumbs->push('User Managment','admin/user-management');
        $this->breadcrumbs->push('User Detail','admin/user-detail');
        $id=$this->uri->segment(3);
        $this->db->where('t_id',$id);
        $data['pagetitle']="User Detail";
        $data['row']=$this->db->get('users_tbl')->row_array();
        $data['loginame']=$this->session->userdata('real_name');
        $data["sidebar_menu"]="admin/template/sidebar_menu";
        $data['main_content']='admin/detail_show';
        $this->load->view('admin/template/admin_template',$data);
    }

    public function add_user()
    {
        $this->breadcrumbs->push('Home','admin');
        $this->breadcrumbs->push('User Managment','admin/user-management');
        $this->breadcrumbs->push('Add User','admin/add-user');
        $data['query']=$this->db->get('users_tbl')->result_array();
        $data['pagetitle']="Add User";
        $data['loginame']=$this->session->userdata('real_name');
        $data["sidebar_menu"]="admin/template/sidebar_menu";
        $data['main_content']='admin/add_user_form';
        $this->load->view('admin/template/admin_template',$data);
    }

    public function add_user_process()
    {
        $first_name=$this->input->post('first_name');
        $last_name=$this->input->post('last_name');
        $username=$this->input->post('username');
        $password=$this->input->post('password');
        $user_role=$this->input->post('user_role');
        date_default_timezone_set("Asia/Rangoon");
        $time =  strtotime(date("Y-m-d H:i:s"));
        $user_id=$this->db->count_all('users_tbl');
        $data = array(
            'user_id'=>$user_id,
            'username' => $username ,
            'first_name' => $first_name ,
            'last_name' => $last_name ,
            'password'=> md5($password),
            'user_role'=> $user_role,
            'creat_date'=> $time,
            'creator_name'=> $this->session->userdata('real_name')
        );
        $this->db->insert('users_tbl',$data);
        $data = array
        (
            'user_id' => $this->session->userdata('user_id'),
            'username' => $this->session->userdata('username'),
            'real_name' => $this->session->userdata('real_name'),
            'About' => 'User Managment',
            'description' => 'Add New',
            'date_time' => $time
        );
        $this->db->insert('activity_log_tbl',$data);
        redirect('admin/user-management');

    }

    public function edit_user()
    {
        $this->breadcrumbs->push('Home','admin');
        $this->breadcrumbs->push('User Managment','admin/user-management');
        $this->breadcrumbs->push('Edit User','admin/edit-user');
        $id=$this->uri->segment(3);
        $this->db->where('t_id',$id);
        $data['pagetitle']="Edit User";
        $data['row']=$this->db->get('users_tbl')->row_array();
        $data['loginame']=$this->session->userdata('real_name');
        $data["sidebar_menu"]="admin/template/sidebar_menu";
        $data['main_content']='admin/edit_user_form';
        $this->load->view('admin/template/admin_template',$data);
    }


    public function edit_user_process()
    {
        $id=$this->uri->segment(3);
        $first_name=$this->input->post('first_name');
        $last_name=$this->input->post('last_name');
        $username=$this->input->post('username');
        $password=$this->input->post('password');
        $user_role=$this->input->post('user_role');
        date_default_timezone_set("Asia/Rangoon");
        $time =  strtotime(date("Y-m-d H:i:s"));
        $data = array(
            'username' => $username ,
            'first_name' => $first_name ,
            'last_name' => $last_name ,
            'password'=> md5($password),
            'user_role'=> $user_role,
            'creat_date'=> $time,
            'creator_name'=> $this->session->userdata('real_name')
        );
        $this->db->where('t_id',$id);
        $this->db->update('users_tbl',$data);
        $data = array
        (
            'user_id' => $this->session->userdata('user_id'),
            'username' => $this->session->userdata('username'),
            'real_name' => $this->session->userdata('real_name'),
            'About' => 'User Managment',
            'description' => 'Edit',
            'date_time' => $time
        );
        $this->db->insert('activity_log_tbl',$data);
        redirect('admin/user-management');
    }

    public function delete_user()
    {
        $id=$this->uri->segment(3);
        $this->db->where('t_id',$id);
        $this->db->delete('users_tbl');
        date_default_timezone_set("Asia/Rangoon");
        $time =  strtotime(date("Y-m-d H:i:s"));
        $data = array
        (
            'user_id' => $this->session->userdata('user_id'),
            'username' => $this->session->userdata('username'),
            'real_name' => $this->session->userdata('real_name'),
            'About' => 'Country',
            'description' => 'Delete',
            'date_time' => $time
        );
        $this->db->insert('activity_log_tbl',$data);
        redirect('admin/user-management');
    }



    public function delete_all(){

        $arr= $this->input->post('arr');

        for ($x = 0; $x < sizeof($arr); $x++) {
            $this->main_model->delete_all($arr[$x]);
        }
        $this->breadcrumbs->push('Home','admin/index');
        $data['loginame']=$this->session->userdata('real_name');
        $data['user_role']=$this->session->userdata('user_role');
        $data['pagetitle']="Customer Orders";
        $data["sidebar_menu"]="admin/template/sidebar_menu";
        $data["query"]=$this->db->query("SELECT o.t_id,o.first_name,o.last_name,o.ph_no,o.address,o.city,o.create_date,o.situation,oi.product_id,GROUP_CONCAT(oi.product_id  SEPARATOR ',' ) as Pname,GROUP_CONCAT(oi.color  SEPARATOR ', ' ) as color,SUM(oi.quantity) as quantity FROM order_list_tbl as o INNER JOIN order_items_tbl as  oi ON oi.order_id = o.t_id WHERE o.disable_at = 1 GROUP BY oi.order_id ORDER BY o.create_date DESC  ")->result_array();
        $data['main_content']='admin/home';
        $this->load->view('admin/home',$data);
    }

    public function get_delete_all(){

        $this->load->library('datatables');
        $this->datatables
            ->select("order_list_tbl.t_id as id,image_name,name,color,item_code,first_name as first_name, last_name , ph_no, order_list_tbl.city,FROM_UNIXTIME(order_list_tbl.create_date),situation " , FALSE)
            ->from('order_list_tbl')
            ->unset_column('situation')
            ->unset_column('t_id')
            ->unset_column('last_name')
            ->join('order_items_tbl', 'order_list_tbl.t_id=order_items_tbl.order_id', 'left')
            ->join('product_tbl', 'product_tbl.t_id=order_items_tbl.product_id', 'left')
            ->group_by("order_items_tbl.order_id ");

        $del='<input id="delAll" name="delValue" value="$1" onchange="valueChanged()" type="checkbox">';
        $image ="<img width=\"50\" src='upload/$1' class='img-responsive'/>";
        $action = '<a data-toggle="tooltip" title="More Info" href="admin/voucher_detail/$1"><i class="fa fa-file-text-o"></i> </a> | <a data-toggle="tooltip" title="Edit"  href="admin/edit-order/$1"><i class="fa fa-pencil-square-o"></i> </a> | <a data-toggle="tooltip" title="Delete" href="admin/delete-order" id="$1" class="delete_order"><i class="fa fa-trash-o"></i> </a>';
        $situation = '<div id="showSelect-$2"> <div tid="$2" id="callStatusUpdate" val="$1" class="status color-$1">$3</div><span id="selectedbox"></span></div>';
        $this->datatables->edit_column("t_id",$del,'id');
        $this->datatables->edit_column("image_name",$image,'image_name');
        $this->datatables->edit_column("first_name",'$1 $2','first_name,last_name');
//        $this->datatables->edit_column("create_date",'$1','date_time(create_date)');
        $this->datatables->add_column("situation",$situation,'situation,id,situation(situation)');
        $this->datatables->add_column("action",$action,'id');
        echo $this->datatables->generate('json', 'ISO-8859-1');

    }



   

   



}