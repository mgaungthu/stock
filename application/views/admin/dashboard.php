<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>

<div class="row">
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">

                <?php
                  $co=$this->db->query("SELECT COUNT(t_id) AS count FROM product_tbl ;  ")->row_array();


                ?>

                <h3><?=$co['count']?></h3>
                <p>Active Product</p>
            </div>
            <div class="icon">
                <i class="fa fa-bar-chart"></i>
            </div>
            <a href="product" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div><!-- ./col -->

    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <?php

                  $cot=$this->db->query("SELECT  count(t_id) as t_id FROM color_tbl WHERE quantity = 0 ;  ")->row_array();


                ?>

                <h3><?=$cot['t_id']?></h3>
                <p>Out of stock</p>
            </div>
            <div class="icon">
                <i class="fa  fa-warning"></i>
            </div>
            <a href="product/out-of-stock" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div><!-- ./col -->

    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
            <div class="inner">
                <?php
                $c=$this->db->query("SELECT COUNT(cid) AS count FROM category_tbl ;  ")->row_array();
                ?>
                <h3><?=$c['count']?></h3>
                <p>Categories</p>
            </div>
            <div class="icon">
                <i class="fa fa-file-archive-o"></i>
            </div>
            <a href="admin/categories" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div><!-- ./col -->

    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-blue">
            <div class="inner">
                <?php
                $five=$this->db->query("SELECT  count(t_id) as t_id FROM color_tbl WHERE quantity < 5 ;  ")->row_array();
                ?>
                <h3><?=$five['t_id']?></h3>
                <p>Below of 5 Qty</p>
            </div>
            <div class="icon">
                <i class="fa fa-thumbs-o-down"></i>
            </div>
            <a href="product/below_stock" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div><!-- ./col -->

    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-fuchsia">
            <div class="inner">
                <?php
                $ret=$this->db->query("SELECT COUNT(t_id) AS count FROM product_tbl WHERE p_type = 2 ;  ")->row_array();
                ?>
                <h3><?=$ret['count']?></h3>
                <p>Return</p>
            </div>
            <div class="icon">
                <i class="fa fa-level-down"></i>
            </div>
            <a href="product/return_item" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div><!-- ./col -->

    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-light-blue-gradient">
            <div class="inner">
                <?php
                $ret1=$this->db->query("SELECT COUNT(t_id) AS count FROM product_tbl WHERE p_type = 3 ;  ")->row_array();
                ?>
                <h3><?=$ret1['count']?></h3>
                <p>Return Damage</p>
            </div>
            <div class="icon">
                <i class="fa fa-fire"></i>
            </div>
            <a href="product/return_damage" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div><!-- ./col -->

    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-light-blue">
            <div class="inner">
                <?php
                $ret2=$this->db->query("SELECT COUNT(t_id) AS count FROM product_tbl WHERE p_type = 4 ;  ")->row_array();
                ?>
                <h3><?=$ret2['count']?></h3>
                <p>Return To Repair</p>
            </div>
            <div class="icon">
                <i class="fa fa-life-ring"></i>
            </div>
            <a href="product/return_repair" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div><!-- ./col -->



    <div class="col-lg-2 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-teal">
            <div class="inner">
                <?php

                $count = $this->db->query(' SELECT COUNT(situation) AS count FROM order_list_tbl WHERE (disable_at = 3 OR situation = 0 ) ')->row_array();
               
                ?>
                <h3><?=$count['count']?></h3>
                <p>Voucher</p>
            </div>
            <div class="icon">
                <i class="fa fa-file-text-o"></i>
            </div>
            <a href="admin/voucher" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>


    <div class="col-lg-2 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <?php

                $count = $this->db->query(' SELECT COUNT(situation) AS count FROM order_list_tbl WHERE situation = 1 ')->row_array();
                ?>
                <h3><?=$count['count']?></h3>
                <p>Already Sent</p>
            </div>
            <div class="icon">
                <i class="fa fa-send"></i>
            </div>
            <a href="admin/already-sent" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <div class="col-lg-2 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-blue">
            <div class="inner">
                <?php

                $count = $this->db->query(' SELECT COUNT(situation) AS count FROM order_list_tbl WHERE situation = 2 ')->row_array();
                ?>
                <h3><?=$count['count']?></h3>
                <p>Already Called</p>
            </div>
            <div class="icon">
                <i class="fa fa-mobile-phone"></i>
            </div>
            <a href="admin/already-called" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div><!-- ./col -->

    <div class="col-lg-2 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <?php

                $count = $this->db->query(' SELECT COUNT(situation) AS count FROM order_list_tbl WHERE situation = 3 ')->row_array();
                ?>
                <h3><?=$count['count']?></h3>
                <p>Can`t Called</p>
            </div>
            <div class="icon">
                <i class="fa  fa-thumbs-o-down"></i>
            </div>
            <a href="admin/cant-called" class="small-box-footer">
                More info <i class="fa fa-arrow-circle-right"></i>
            </a>
        </div>
    </div><!-- ./col -->

    <div class="col-lg-2 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-gray">
            <div class="inner">
                <?php

                $count = $this->db->query(' SELECT COUNT(situation) AS count FROM order_list_tbl WHERE situation = 4 ')->row_array();
                ?>
                <h3><?=$count['count']?></h3>
                <p>Canceled</p>
            </div>
            <div class="icon ">
                <i class="fa fa-close"></i>
            </div>
            <a href="admin/cancel" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <div class="col-lg-2 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <?php
                $this->db->where('disable_at',1);
                $count=$this->db->count_all('order_list_tbl');
                ?>
                <h3><?=$count?></h3>
                <p>Total Orders</p>
            </div>
            <div class="icon">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <a href="admin" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
</div>
</div>
