 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 $uri = $this->uri->segment(2);
 if($uri == '' || $uri == 'index'){
     $uri='table';
 }
 else{
     $uri='get_'.$uri;
 }

 ?>
 
<script>
    $(document).ready(function()
    {
        $('#PRData').DataTable( {
            "processing": true,
            "serverSide": true,
            "language": {
                "processing": "Processing..."
            },
            "ajax": {
                "url": "admin/<?=$uri?>",
                "type": "POST"
            },
            "columnDefs": [{
                'orderable': true,
                'targets': [0,3]
            }],
            "order": [[ 1, "DESC" ]],
            "Columns":
                [
                    { "data": "first_name", "name": "order_list_tbl.first_name" },
                    { "data": "last_name", "name": "order_list_tbl.last_name" },
                    { "data": "ph_no", "name": "order_list_tbl.ph_no" },
                    { "data": "address", "name": "order_list_tbl.address" },
                    { "data": "city", "name": "order_list_tbl.city" },
                    { "data": "create_date", "name": "order_list_tbl.create_date" },


                ]
        } );
    });
</script>
 <div id="reload">

<br>
     <?php if($user_role==1):?>
         <a style="margin-left: 10px" class="btn btn-sm btn-primary" id="ckAll">Select All</a>
         <a style="margin-left: 10px;display: none;" class="btn btn-sm btn-danger" id="deckAll">Deselect All</a>
     <span style="display: none;" id="loadinContent">
         Completed
     </span>
 <a link="<?=base_url()?>admin/delete_all" id="delConfirm" style="margin-right: 10px;display: none" class="btn btn-sm btn-danger pull-right" >Delete All</a>
     <?php
     endif;
     ?>
                <div class="box-body">
             <div class="table-responsive">
                  <table id="PRData" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                          <th></th>
                          <th>Image</th>
                          <th>Product Name</th>
                          <th>Color</th>
                          <th>Item Code</th>
                          <th>Customer Name</th>
                          <th>Ph no</th>
                          <th>City</th>
                          <th>Order Date</th>
                          <th data-sortable="false">Situation</th>
                          <th data-sortable="false">Action</th>

                      </tr>
                    </thead>
                    <tbody>

                    </tbody>

                    </tfoot>
                  </table>
                  </div>
                    <?php
                    if($this->session->userdata('user_role')==1||$this->session->userdata('user_role')==2):
                        ?>
                        <div class="box-footer">
                            <a href="admin/add-order" class="btn btn-sm btn-info btn-flat pull-left">Add New</a>
                        </div>
                        <?php
                    endif;
                    ?>
                </div><!-- /.box-body -->
 </div>
 
 