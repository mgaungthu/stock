<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="box">
    <div class="box-body">

        <div class="container bigpadding">

            <div class="col-md-6 col-md-push-1 smalltopmargin">
                <?php
                if (!empty($this->uri->segment(3))) :
                    ?>
                    <?=form_open("admin/edit-category-proccess/".$this->uri->segment(3),'')?>
                <?php else:?>
                    <?=form_open("admin/add-category-proccess",'')?>
                <?php endif;?>
                <section class="row">
                    <div class="col-md-3 smallpadding midsidepadding">
                        <?=form_label('Category :')?>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <?=form_input("category",@$row["name"],"placeholder='Category' class='form-control' required")?>
                        </div>
                    </div>

                </section>





                <div class="modal-footer">
                   <?=form_submit("Save","Save","class='btn btn-primary'")?>
                </div>



            </div>


            <div class="col-md-2">
            </div>
        </div>
        <?=form_close();?>

    </div>
</div>
