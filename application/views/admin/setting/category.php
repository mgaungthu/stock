 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="box">
    <div class="box-body">
        <?php
        if($this->session->userdata('user_role')==1):
        ?>
        <div class="pull-right">
            <a href="admin/add-new-category" class="btn btn-primary">Add New</a>
        </div>
        <?php
        endif;
        ?>
    <div class="col-md-push-1 col-md-6">
        <ul class='category'>
        <?php
        foreach($query as $row):
        ?>

        <li class="rowll rowll-<?=$row['cid']?>">
            <?php
            $co=0;
            foreach($count as $res){
                if($row['cid']==$res['category']){

                    $co += $res['quantity'];

                }

            }
            ?>
            <a href="admin/category-filter/<?=$row['cid']?>"><?=$row['name']?></a> (<?=$co?>)
            <?php
            if($this->session->userdata('user_role')==1):
                ?>
            <div class='pull-right'><a href='admin/edit-category/<?=$row['cid']?>'>Edit</a>|<a url='<?=base_url()?>admin/delete-cat' val='<?=$row['cid']?>' id='deleteCat' href='#'>Delete</a></div>
                <?php
                endif;
                ?>
        </li>


        <?php
        endforeach;
        ?>

        </ul>
    </div>
    </div><!-- /.box-body -->
</div>