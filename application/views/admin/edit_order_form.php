 <div class="box-body">


  <?php
  if (!empty($this->uri->segment(3))) :
  ?>
<?=form_open("transfer/edit_transfer_process/".$this->uri->segment(3),'')?>
<?php else:?>
    <?=form_open("transfer/add_transfer_process",'')?>
<?php endif;?>



         <div class="col-md-12">
             <div class="form-group">
                 <?=form_label('Delivery Name :')?>
                 <select name="delivery_name" id="delivery_name" data-placeholder="Choose one" class="chosen-select" tabindex="2" required>
                     <option value=""></option>
                     <option value="9983" <?php if(9983==@$row["delivery_name"]){echo 'selected';} ?>>Walk in Customer</option>
                     <?php
                     $this->db->order_by('first_name','DESC');
                     $this->db->where('user_role',3);
                     $q=$this->db->get('users_tbl')->result_array();
                     foreach($q as $key=>$deli):
                         ?>
                         <option value="<?=$deli['t_id']?>" <?php if($deli['t_id']==@$row["delivery_name"]){echo 'selected';} ?> ><?=$deli['first_name']?> <?=$deli['last_name']?></option>
                         <?php
                     endforeach;
                     ?>
                 </select>
             </div>
         </div>


     <?php
     $i=1;
     $n=0;
     $this->db->where('order_id',$row["t_id"]);
     $data=$this->db->get('order_items_tbl')->result_array();
     foreach ($data as $result):
         $n++;
     ?>
     <section id="row-<?=$result['id']?>" class="mutiProduct">


         <div class="col-md-12">
             <ul class="list-inline pull-right">
                 <?php
                 if($this->session->userdata('user_role')==1||$this->session->userdata('user_role')==2):
                 if($n!=1):
                 ?>
                 <li>
                     <a href="<?=base_url()?>admin/delete_order_product" tid="<?=$result['id']?>" id="del_order" ><i class="fa fa-lg fa-trash"></i></a>

                 </li>
                     <?php
                 endif;
                 endif;
                 ?>
             </ul>
             <div class="form-group">
                 <?php
                 if($i==1):
                     $i++;
                     ?>
                     <?=form_label('Product Name :')?>
                     <?php
                 endif;
                 ?>
                 <select name="product_name[]" id="productName<?=$result['id']?>" onchange="mutiproductname('<?=$result['id']?>')" data-placeholder="Choose one" class="chosen-select" tabindex="2" required>
                     <option value=""></option>
                     <?php

                     $query=$this->db->get('product_tbl')->result_array();
                     foreach($query as $key=>$val):
                         ?>
                         <option value="<?=$val['t_id']?>" <?php if($val['t_id']==$result["product_id"]){echo 'selected';} ?> ><?=$val['name']?> (<?=$val['item_code']?>)</option>
                         <?php
                     endforeach;
                     ?>
                 </select>

             </div>

         </div>

         <div id="item_list<?=$result['id']?>">
             <?php
             $id = urldecode($this->uri->segment(3));
             $this->db->order_by('color',"DESC");
             $this->db->where('product_id',$result['product_id']);
             $get=$this->db->get('color_tbl')->result_array();

             ?>
             <input id="RemainUrl" type="hidden" value="<?=base_url()?>transfer/remaining_items">
             <div>
                 <section class="row">
                     <div class="col-md-3">

                     </div>
                     <div class="col-md-9 text-center">
                         <img class="img-responsive smallpadding" width="150" height="123" src="upload/<?php if ($this->main_model->getName($result['product_id'],'img')==null){echo 'default-product.jpg';}elseif($this->main_model->getName($result['product_id'],'img')=='OTS'){echo "out-of-stock.jpg";}else{echo $this->main_model->getName($result['product_id'],'img');}?>"  />

                     </div>

                 </section>
                 <section class="row">
                     <div class="col-md-3 smallpadding midsidepadding">
                         <?=form_label('Color  :')?>
                     </div>
                     <div class="col-md-5">
                         <div class="form-group">
                             <select name="color[]" id="color<?=$result['id']?>" onchange="mutitColor('<?=$result['id']?>')" data-placeholder="Choose Color" class="chosen-select" tabindex="2" required>
                                 <option value="0"></option>
                                 <?php foreach($get as $key=>$colo): ?>
                                     <option value="<?=$colo["color"]?>" <?php if($colo['color']==$result["color"]){echo 'selected';} ?>  ><?=$colo["color"]?></option>
                                     <?php
                                 endforeach;
                                 ?>
                             </select>
                         </div>
                     </div>
                     <div class="col-md-4">
                         Remaining Items :
                         <span id="remainItem<?=$result['id']?>">
                         <?php
                         $colorName = $result["color"];
                         $productId = $result["product_id"];

                         $get=$this->db->query("SELECT SUM(quantity) as count FROM color_tbl WHERE color = '$colorName' AND product_id = '$productId' ")->row_array();
                         $this->db->where('t_id',$productId);
                         $vtow=$this->db->get('product_tbl')->row_array();
                         ?>

                             <b><?=$get['count']?></b>

                         </span>
                     </div>
                 </section>
                 <section class="row">
                     <div class="col-md-3 smallpadding midsidepadding">
                         <?=form_label('Quantity :')?>
                     </div>
                     <div class="col-md-9">
                         <div class="form-group">
                             <input type="number" name="quantity[]" value="<?=$result['quantity']?>" placeholder='Quantity' class='form-control' required>
                         </div>
                     </div>
                 </section>
             </div>
         </div>

     </section>

     <?php
     endforeach;
     ?>

     <div id="item_list">
        
     </div>

     <div id="muti_product">

     </div>
    
     <div class="col-md-12">
         <span class="pull-right" id="add_pname"><i class="fa fa-plus"></i></span>
     </div>
    
     <input type="hidden" id="MutiProductUrl" value="<?=base_url()?>transfer/mutiproduct">
     <input id="product_url" type="hidden" value="<?=base_url()?>transfer/product_detail">


         <h3 class="smallpadding"> Customer Information </h3>

      <div class="col-md-12">
        <div class="form-group">
            <?=form_label('First Name :')?>
        <?=form_input("first_name",@$row["first_name"],"placeholder='First Name' class='form-control' required")?>
        </div>
      </div>
       <div class="col-md-12">
        <div class="form-group">
            <?=form_label('Last Name :')?>
        <?=form_input("last_name",@$row["last_name"],"placeholder='Last Name' class='form-control' ")?>
        </div>
      </div>


      <div class="col-md-12">
        <div class="form-group">
            <?=form_label('Ph number')?>
         <input type="number" value="<?=@$row["ph_no"]?>" name="ph_no" placeholder='Phone number' class='form-control' required")?>

        </div>
      </div>

      <div class="col-md-12">
        <div class="form-group">
            <?=form_label('Address')?>
            <textarea name="address" class="form-control" rows="3"><?=$row["address"]?></textarea>

      </div>
      </div>


      <div class="col-md-12">
        <div class="form-group">
            <?=form_label('Note Pad')?>

            <textarea name="note_pad" class="form-control" rows="3"><?=$row["note_pad"]?></textarea>
      </div>
      </div>


          <div class="col-md-12">
            <div class="form-group">
                <?=form_label('City :')?>
            <?=form_input("city",@$row["city"],"placeholder='City' class='form-control' required")?>
          </div>
          </div>





      <div class="modal-footer">
         <?=form_submit("Save","Save","class='btn btn-primary'")?>
      </div>





  <div class="col-md-2">
  </div>
</div>
<?=form_close();?>
