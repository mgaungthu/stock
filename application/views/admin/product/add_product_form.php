<div class="box">
 <div class="box-body">

 <div class="col-md-12  smalltopmargin">
  <?php
  if (!empty($this->uri->segment(3))) :
  ?>
<?=form_open_multipart("product/edit-order-process/".$this->uri->segment(3),'')?>
<?php else:?>
    <?=form_open_multipart("product/add-product-process",'')?>
<?php endif;?>

    <div class="col-md-12">
        <div class="form-group">
            <?=form_label('Type')?>
            <?php
            $options = array(
                ''       => 'Choose One',
                '1'      => 'New',
                '2'      => 'Return',
                '3'      => 'Return Damage',
                '4'      => 'Return To Repair',
            );
            ?>
            <?=form_dropdown('p_type', $options,'','class="form-control" required')?>
        </div>
    </div>


         <div class="col-md-12">
             <div class="form-group">
                 <?=form_label('Product')?>
                 <?=form_input("product",@$row["product"],"placeholder='Product' class='form-control' required")?>
             </div>
         </div>


         <div class="col-md-12">
             <div class="form-group">
                 <?=form_label('Category')?>
                 <select data-placeholder="Choose Category" class="chosen-select" name="category">
                     <option value=""></option>
                     <?php
                     foreach($cat as $key => $op):
                         ?>
                         <option value="<?=$op['cid']?>" ><?=$op['name']?></option>
                     <?php endforeach;?>
                 </select>
             </div>
         </div>



         <div class="col-md-12">
             <div class="form-group">
                 <?=form_label('Item Code')?>
                 <?=form_input("item_code",@$row["item_code"],"placeholder='Item Code' class='form-control' required")?>
             </div>
         </div>



         <div class="col-md-12">
             <div class="form-group">
                 <?=form_label('Purchase Price')?>
                 <?=form_input("price",@$row["price"],"placeholder='Purchase Price' class='form-control' required")?>
             </div>
         </div>


        <div class="col-md-12">
            <div class="form-group">
                <?=form_label('Selling Price')?>
                <?=form_input("s_price",@$row["s_price"],"placeholder='Selling Price' class='form-control' required")?>
            </div>
        </div>




         <div class="col-md-12">
             <div class="form-group">
                 <?=form_label('Color')?>
                 <?=form_input("color[]",@$row["color"],"placeholder='Color' class='form-control' ")?>
             </div>
         </div>


         <div class="col-md-12">
             <div class="form-group">
                 <input type="number" name="quantity[]" value="<?=@$row["quantity"]?>" placeholder='Quantity' class='form-control' required>

             </div>
         </div>


     <div id="MoreWrapper">

     </div>

    <div class="col-md-12">
         <span class="pull-right" id="add_More"><i class="fa fa-plus"></i></span>
     </div>

     <div class="preview col-md-12 countdiv">
         <img width="175px" height="130px" id="preview" src="images/default-product.jpg" class="img-responsive">
         <div class="fileUpload btn btn-info" >
             <span>Upload</span>
             <?=form_upload('userfile','default-product.jpg','class="upload" onchange="readURL(this);"')?>
         </div>
     </div>

      <div class="modal-footer">              
         <?=form_submit("Save","Save","class='btn btn-primary'")?>
      </div>


 
</div>
        
      
  <div class="col-md-2">
  </div>
</div>
<?=form_close();?>
</div>