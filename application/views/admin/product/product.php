 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
 
 <?php setlocale(LC_MONETARY,"en_US"); ?>
<div class="box">
    <div class="box-body">
        <table id="stockList" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>No</th>
                <th>Product Status</th>
                <th>Category</th>
                <th>Image</th>
                <th>Name</th>
                <th>Color</th>
                <?php if($user_role==1):?>
                <th>Purchase Price</th>
                <?php endif;?>
                <th>Selling Price</th>
                <th>Code</th>
                <th>Remaining Quantity</th>
                <th>Total Sold</th>
                <th>Creat Date</th>
                <th>Who Create</th>
                <?php if($user_role==1):?>
                <th>Action</th>
                <?php endif;?>
            </tr>
            </thead>

            <tbody>

            <?php
            $no=1;
            foreach($query as $key=>$row):
            ?>
            <tr id="row-<?=$row["t_id"]?>">
                <td><?=$no++?></td>
                <td><?=p_type($row['p_type'])?></td>
                <td><?=$this->main_model->category($row['category'])?></td>
                <td><img width="50" src="upload/<?php if($row['image_name']==null){echo 'default-product.jpg';}else{echo $row['image_name'];}?>" class="img-responsive"></td>
                <td><?=$row['name']?></td>
                <td><?=$row['color']?></td>
                <?php if($user_role==1):?>
               <td>
               	<?php $check = $row['price']?>
               	<?php if ($check != NULL):
               		echo $check  . ' Ks';
               		endif;
               	?>
                </td>
                <?php
                endif;
                ?>
                <td>
                	<?php $selling_price = $row['s_price']?>
                	<?php if ($selling_price != NULL):
               		echo $selling_price . ' Ks';
               		endif;
               	?>
                </td>
                <td><?=$row['item_code']?></td>
                <td>
                    <?php
                    echo $row['quantity'];
                    ?></td>
                <td><?php
                    $count=0;
                    foreach($trans as $key=>$tr){
                        if($tr->p_id==$row['product_id']) {

                            $count=$tr->quantity;

                        }
                    }
                    echo $count;
                    ?></td>
                <td><?=date_time($row['creat_date'])?></td>
                <td><?=$row['who_created']?></td>
                <?php if($user_role==1):?>
                <td>
                    <a  data-toggle="tooltip" title="More Info" href="product/edit-product/<?=$row["t_id"]?>"><i class="fa fa-file-text-o"></i> </a>
                    <a data-toggle="tooltip" title="Delete" href="product/delete-product" id="<?=$row["t_id"]?>" class="delete_product"><i class="fa fa-trash-o"></i> </a>
                </td>
                <?php endif;?>
            </tr>
            <?php
            endforeach;
            ?>
            </tbody>
        </table>
    </div><!-- /.box-body -->
    <?php
    if($this->session->userdata('user_role')==1||$this->session->userdata('user_role')==2):
    ?>
    <div class="box-footer">
        <a href="product/add-new-product" class="btn btn-sm btn-info btn-flat pull-left">Add New</a>
    </div>
    <?php
    endif;
    ?>
</div>