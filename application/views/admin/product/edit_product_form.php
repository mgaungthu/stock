<div class="box">
    <div class="box-body">

        <div class="col-md-12  smalltopmargin">
  <?php
  if (!empty($this->uri->segment(3))) :
  ?>
<?=form_open_multipart("product/edit-product-process/".$this->uri->segment(3),'')?>
<?php else:?>
    <?=form_open_multipart("product/add-product-process",'')?>
<?php endif;?>


     <div class="col-md-12">
         <div class="form-group">
             <?=form_label('Type')?>
             <?php
             $options = array(
                 ''       => 'Choose One',
                 '1'      => 'New',
                 '2'      => 'Return',
                 '3'      => 'Return Damage',
                 '4'      => 'Return To Repair',
             );
             ?>
             <?=form_dropdown('p_type', $options,@$row["p_type"],'class="form-control" required')?>
         </div>
     </div>


         <div class="col-md-12">
             <div class="form-group">
                 <?=form_label('Product')?>
                 <?=form_input("product",@$row["name"],"placeholder='Product' class='form-control' required")?>
             </div>
         </div>


     <div class="col-md-12">
         <div class="form-group">
             <?=form_label('Category')?>
             <select  data-placeholder="Choose Category" class="chosen-select" name="category">
                 <option value=""></option>
                 <?php
                 foreach($cat as $key => $op):
                 ?>
                 <option value="<?=$op['cid']?>" <?php if($op['cid']==$row['category']){ echo 'selected';} ?> ><?=$op['name']?></option>
                 <?php endforeach;?>
             </select>
         </div>
     </div>


      <div class="col-md-12">
        <div class="form-group">
            <?=form_label('Item Code')?>
        <?=form_input("item_code",@$row["item_code"],"placeholder='Item Code' class='form-control' required")?>
      </div>
      </div>

      <div class="col-md-12">
        <div class="form-group">
        <?=form_input("purchase_price",@$row["price"],"placeholder='Purchase Price' class='form-control' required")?>
      </div>
      </div>



      <div class="col-md-12">
         <div class="form-group">
        <?=form_label('Selling Price')?>
        <?=form_input("selling_price",@$row["s_price"],"placeholder='Selling Price' class='form-control' required")?>
      </div>
      </div>


     <div id="MoreWrapper">
         <?php

         $this->db->order_by('color',"DESC");
         $this->db->where('product_id',$row['t_id']);
         $get=$this->db->get('color_tbl')->result_array();
         foreach($get as $key=>$colo):
             ?>
         <div>

             <div class="col-md-12">
                 <div class="form-group">
                     <?=form_label('Color')?>
                     <?=form_input("preview[]",@$colo["color"],"placeholder='Color' class='form-control' readonly")?>
                 </div>
             </div>
             <div id="<?=$colo["t_id"]?>" class="col-md-12">
                 <div id="ajax_response">

                 </div>
                 <div class="form-group">
                     <?php
                     $minus=$colo['quantity'];
//                     foreach($trans as $key=>$tr){
//                         if($tr->p_id==$colo['product_id'] && $tr->color==$colo['color'] ){
//
//                                 $minus=$tr->quantity;
//                                 $minus= $colo['quantity']-$minus;
//
//                         }
//                     }

                     ?>
                     <input id="quantity"  type="number" name="preview[]" value="<?=$minus?>" placeholder='Quantity' class='form-control' required>
                     <?php
                     if($this->session->userdata('user_role')==1||$this->session->userdata('user_role')==2):
                     ?>
                     <a  onclick="QuanityUpdate(<?=$colo["t_id"]?>,<?=$row["t_id"]?>,'<?=$colo["color"]?>')"  id="saveQ" class="btn btn-sm btn-danger smalltopmargin">Save</a>
                     <?php
                     endif;
                     ?>
                 </div>
             </div>
             <?php
         endforeach;
         ?>

             <?php
             if($this->session->userdata('user_role')==1||$this->session->userdata('user_role')==2):
             ?>
             <span tid="<?=$colo["t_id"]?>" style="bottom: 165px;" id="Remove_btn"><i class="fa  fa-trash"></i> </span></div>
             <?php
             endif;
             ?>
     </div>
            <input id="updateColorUrl" type="hidden" value="<?=base_url()?>product/update_color_q"/>
            <input type="hidden" value="<?=base_url()?>product/delete_color" id="delurl">

     <div class="col-md-12">
         <?php
         if($this->session->userdata('user_role')==1||$this->session->userdata('user_role')==2):
         ?>
         <span class="pull-right" id="add_More"><i class="fa fa-plus"></i></span>
         <?php
         endif;
         ?>
     </div>
     
     <div class="preview col-md-12 countdiv">
         <img width="175px" height="130px" id="preview" src="upload/<?php if($row['image_name']==null){echo 'default-product.jpg';}else{echo $row['image_name'];}?>" class="img-responsive">
         <div class="fileUpload btn btn-info" >
             <span>Upload</span>
             <?php if($row['image_name']==null){$imval= 'default-product.jpg';}else{$imval= $row['image_name'];}?>
             <?=form_upload('userfile',$imval,'class="upload" onchange="readURL(this);"')?>
         </div>


     </div>
     <?php
     if($this->session->userdata('user_role')==1||$this->session->userdata('user_role')==2):
     ?>
      <div class="modal-footer">
         <?=form_submit("Save","Save","class='btn btn-primary'")?>
      </div>

     <?php
     endif;
     ?>

</div>


  <div class="col-md-2">
  </div>
</div>
    </div>
<?=form_close();?>
