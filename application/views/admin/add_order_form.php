 <div class="box-body">


  <?php
  if (!empty($this->uri->segment(3))) :
  ?>
<?=form_open("admin/edit-order-process/".$this->uri->segment(3),'')?>
<?php else:?>
    <?=form_open("transfer/add_transfer_process",'')?>
<?php endif;?>
<div class="col-md-12">
             <div class="form-group">
                 <?=form_label('Delivery Name')?>
                 <select name="delivery_name" id="delivery_name" data-placeholder="Choose one" class="chosen-select" tabindex="2" required>
                     <option value=""></option>
                     <option value="9983">Walk in Customer</option>
                     <?php
                     $this->db->order_by('first_name','DESC');
                     $this->db->where('user_role',3);
                     $q=$this->db->get('users_tbl')->result_array();
                     foreach($q as $key=>$deli):
                         ?>
                         <option value="<?=$deli['t_id']?>" <?php if($deli['t_id']==@$row["delivery_name"]){echo 'selected';} ?> ><?=$deli['first_name']?> <?=$deli['last_name']?></option>
                         <?php
                     endforeach;
                     ?>
                 </select>
             </div>
        </div>

         <div class="col-md-12">
             <div class="form-group">
                 <?=form_label('Product Name')?>
                 <select name="product_name[]" id="product_name" data-placeholder="Choose one" class="chosen-select" tabindex="2" required>
                     <option value=""></option>
                     <?php

                     $query=$this->db->get('product_tbl')->result_array();
                     foreach($query as $key=>$val):
                         ?>
                         <option value="<?=$val['t_id']?>" >
                             <?=$val['name']?> (<?=$val['item_code']?>)
                         </option>
                         <?php
                     endforeach;
                     ?>
                 </select>

             </div>

         </div>


     <div id="item_list">
        
     </div>

     <div id="muti_product">

     </div>
    
     <div class="col-md-12">
         <span class="pull-right" id="add_pname"><i class="fa fa-plus"></i></span>
     </div>
    
     <input type="hidden" id="MutiProductUrl" value="<?=base_url()?>transfer/mutiproduct">

     <input id="product_url" type="hidden" value="<?=base_url()?>transfer/product_detail">



         <h3 class="smallpadding"> Customer Information </h3>

      <div class="col-md-12">
          <?=form_label('First Name')?>
        <div class="form-group">
        <?=form_input("first_name",@$row["first_name"],"placeholder='First Name' class='form-control' required")?>
        </div>
      </div>
       <div class="col-md-12">
           <?=form_label('Last Name')?>
        <div class="form-group">
        <?=form_input("last_name",@$row["last_name"],"placeholder='Last Name' class='form-control' ")?>
        </div>
      </div>

     <div class="col-md-12">
        <div class="form-group">
            <?=form_label('Ph number')?>
         <input type="number" value="<?=@$row["ph_no"]?>" name="ph_no" placeholder='Phone number' class='form-control' required")?>

        </div>
      </div>




      <div class="col-md-12">
        <div class="form-group">
            <?=form_label('Address')?>
        <textarea name="address" class="form-control" rows="3"></textarea>
      </div>
      </div>



      <div class="col-md-12">
        <div class="form-group">
            <?=form_label('Note Pad')?>
            <textarea name="note_pad" class="form-control" rows="3"></textarea>

      </div>
      </div>

          <div class="col-md-12">
            <div class="form-group">
                <?=form_label('City')?>
            <?=form_input("city",@$row["city"],"placeholder='City' class='form-control' required")?>
          </div>
          </div>





      <div class="modal-footer">
         <?=form_submit("Save","Save","class='btn btn-primary'")?>
      </div>






  <div class="col-md-2">
  </div>
</div>
<?=form_close();?>
