<?php
date_default_timezone_set("Asia/Rangoon");
$this->db->where('t_id',$id);
$get=$this->db->get('order_list_tbl')->row_array();
?>
<base href="<?=base_url()?>"/>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/AdminLTE.css">
<div class="printWrapper">
    <div class="col-md-12">
        <div id="receipt-data">
            <div class="text-center">
                <img width="63%" src="images/logo.png">
<!--                <h2 style="text-transform:capitalize;"><span style="font-size: 60px;font-family: Helvetica"><b>D</b></span>ouble<span style="font-size: 60px;"><b>Y</b></span>-->
<!--                    <span style="font-size:21px;font-family: Helvetica"><b>International Co., Ltd.</b></span></h2>-->
            <p><strong>Ph: 09  430 44622, 09 402 773838, 09 25 8227787, 09 25 4455595</strong></p>
                <address>No 1253, Yadanar Street, Southokkalapa Township, Yangon, Myanmar, 11060</address>
            </div>
            <hr>
<!--            <div class="col-md-6 nopadding">-->
<!--                <p>Full Name : --><?//=$get['first_name']?><!-- --><?//=$get['last_name']?><!--<br> Address : --><?//=$get['address']?><!--</p>-->
<!--            </div>-->
<!--            <div class="col-md-6 nopadding">-->
<!--               <p class="pull-right"> Reference No: SALE--><?//=date('/Y/m/').$voucher_no?><!--<br> Sales Date: --><?//=date('d/m/Y H:i:s')?><!--</p>-->
<!--            </div>-->
            <table class="table no-border headertb">
                <tbody>
                <tr>
                    <td class="text-left">
                        <p><b>Customer Name</b> : <?=$get['first_name']?> <?=$get['last_name']?></p>
                    </td>
                    <td class="text-right"><p><b>Reference No</b> : SALE<?=date('/Y/m/').$voucher_no?><br> </p></td>
                </tr>
                <tr>
                    <td class="text-left"><b><p>Phone No </b>: <?=$get['ph_no']?></p></td><td class="text-right"><p><b>Sales Date </b>: <?=date('d/m/Y H:i:s')?></p></td>

                </tr>
                <tr>
                    <td colspan="2" class="text-left"><b><p>Address</b> : <span style="margin-left:3px;width: 580px;float: right;"><?=$get['address']?></span></p></td>

                </tr>
                </tbody>
            </table>

            <div style="clear:both;">

            </div>
            <table class="table">
                <thead>
                <th>
                    Sr.No
                </th>
                <th>
                    Stock Name
                </th>
                <th>
                    Selling Price
                </th>
                <th>
                    QTY
                </th>
                <th>
                    Disc
                </th>
                <th>
                    Amount
                </th>
                </thead>
                <tbody>
                <?php
                $alltotlal='';
                foreach ($res as $key=>$row):
                ?>
                <tr>
                    <td class="no-border">#<?=$key+1?></td>
                    <td class="no-border " > <?=$this->main_model->getName($row["product_id"],'pcode')?> (<?=$this->main_model->getName($row["product_id"],'pname')?>) </td>
                    <td class="no-border"><?=number_format($sellPrice[$key],2)?> <span class="pull-right">*Kyat</span></td>
                    <td class="no-border"><?=$row["quantity"]?></td>
                    <td class="no-border"></td>
                    <td class="no-border"> <?php

                        $tol=$row["quantity"] * $sellPrice[$key];
                        $alltotlal +=$tol;
                        echo  number_format($tol,2);
                        ?>
                    </td>
                    <tr>

                <?php
                endforeach;
                ?>

                <?php
                if (count($res)<9):
                for($x=0;$x<8;$x++):
                ?>
                <tr>
                    <td class="no-border"></td>
                    <td class="no-border ">  </td>
                    <td class="no-border"></td>
                    <td class="no-border"></td>
                    <td class="no-border"></td>
                    <td class="no-border"> </td>
                <tr>
                <tr>
                    <td class="no-border"></td>
                    <td class="no-border ">  </td>
                    <td class="no-border"></td>
                    <td class="no-border"></td>
                    <td class="no-border"></td>
                    <td class="no-border"> </td>
                <tr>
                    <?php
                    endfor;
                    endif;
                    ?>
                </tbody>
                <tfoot>
                <?php
                if($paymethod=="Cash"):
                ?>
                <tr>
                    <th></th>
                    <th colspan="3" class="text-right">Total</th>
                    <th></th>
                    <th ><?=number_format($alltotlal,2)?></th>
                </tr>
                    <?php
                    if($discount!=0):
                        $alltotlal=$alltotlal - $discount;
                    ?>
                             <tr>
                                    <th></th>
                                    <th colspan="3" class="text-right">Order Discount</th>
                                    <th></th>
                                    <th ><?=number_format($discount,2)?></th>
                             </tr>
                        <?php
                        endif;
                        ?>
                    <tr>
                        <th></th>
                        <th colspan="3" class="text-right">Grand Total</th>
                        <th></th>
                        <th ><?=number_format($alltotlal,2)?></th>
                    </tr>
                <tr>
                    <th></th>
                    <th colspan="3" class="text-right">Deposit</th>
                    <th></th>
                    <th >0</th>
                </tr>
                <tr>
                    <th></th>
                    <th colspan="3" class="text-right">Balance</th>
                    <th></th>
                    <th >0</th>
                </tr>
                <?php
                else:
                ?>
                    <tr>
                        <th></th>
                        <th colspan="3" class="text-right">Total</th>
                        <th></th>
                        <th ><?=number_format($alltotlal,2)?></th>
                    </tr>
                    <?php
                    if($depdiscount!=0):
                        $alltotlal=$alltotlal - $depdiscount;
                        ?>
                        <tr>
                            <th></th>
                            <th colspan="3" class="text-right">Order Discount</th>
                            <th></th>
                            <th ><?=number_format($depdiscount,2)?></th>
                        </tr>
                        <?php
                    endif;
                    ?>
                    <tr>
                        <th></th>
                        <th colspan="3" class="text-right">Grand Total</th>
                        <th></th>
                        <th ><?=number_format($alltotlal,2)?></th>
                    </tr>
                    <tr>
                        <th></th>
                        <th colspan="3" class="text-right">Deposit 1</th>
                        <th></th>
                        <th ><?=number_format(@$deposit,2)?></th>
                    </tr>
                    <tr>
                        <th></th>
                        <th colspan="3" class="text-right">Deposit 2 </th>
                        <th></th>
                        <th><?=number_format(@$deposit2,2)?></th>
                    </tr>
                    <?php
                   $alltotlal= $alltotlal - $deposit;
                    if($deposit2!=0){
                        $alltotlal=  $deposit2 + $deposit;
                        $alltotlal=0;
                        $payamount= $deposit2 + $deposit;
                    }
                    else{
                        $payamount=$deposit;
                    }
                    ?>
                    <tr>
                        <th></th>
                        <th colspan="3" class="text-right">Balance</th>
                        <th></th>
                        <th ><?=number_format($alltotlal,2)?></th>
                    </tr>
                <?php
                endif;
                ?>
                </tfoot>
            </table>
            <table class="table table-striped table-condensed">
                <tbody>
                <?php
                if($paymethod=="Cash"):
                ?>
                <tr>
                    <td>Paid by : Cash</td>
                    <td class="text-center">Amount: <?=number_format($payamount,2)?></td>
                    <?php
                    $change = $payamount - $alltotlal;
                    $this->db->where('t_id',$row["order_id"]);
                    $this->db->update('order_list_tbl',array('discount'=>$discount,'pay_method'=>'Cash','total_amount'=>$alltotlal,'voucher_date'=>time()));
                    ?>
                    <td class="text-center">Change: <?=number_format($change,2)?></td>
                </tr>
                <?php
                else:
                ?>
                <tr>
                    <td>Paid by : Deposit</td>
                    <td class="text-center">Amount: <?=number_format($payamount,2)?></td>
                    <?php
                    $total_amount = $deposit + $deposit2;
                    $this->db->where('t_id',$row["order_id"]);
                    $this->db->update('order_list_tbl',array('discount'=>$depdiscount,'pay_method'=>'Deposit','deposit_1'=>$deposit,'deposit_2'=>$deposit2,'total_amount'=>$total_amount,'voucher_date'=>time()));
                    ?>
                    <td class="text-center">Balance: <?=number_format($alltotlal,2)?></td>
                </tr>
                <?php
                endif;
                ?>
                </tbody>
            </table>
            <p>* ပစၥည္းလက္ခံခ်ိန္တြင္စစ္ေဆးျပီးယူေပးပါရန္၊ ပစၥည္းျပန္မလဲေပးပါ။</p>
            <div class="well well-sm">
                Thank you for shopping with us. Please come again            </div>
        </div>
        <div class="no-print">
            <a href="javascript:window.print()" id="web_print" class="btn btn-block btn-primary" onclick="window.print();return false;">Print</a>
            <a class="btn btn-block btn-warning" href="admin/voucher_detail/<?=$this->uri->segment(3)?>">Back</a>
        </div>

    </div>

</div>

<script>
    window.onload = function() { window.print(); }
</script>
