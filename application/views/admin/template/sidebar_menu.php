 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<!-- Sidebar Menu -->
          <ul class="sidebar-menu">
            <li class="header">HEADER</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="<?php if($this->uri->segment(2)=='dashboard'){echo'active';}?>"><a href="admin/dashboard"><i class="fa fa-link"></i> <span>Dashboard</span></a></li>
              <li class="treeview <?php if($this->uri->segment(2)=='edit-product'||$this->uri->segment(2)=='add-new-product'||$this->uri->segment(2)=='' || $this->uri->segment(2)=='categories' || $this->uri->segment(2)=='transfer-list'  ){echo'active';}?>">
                  <a href="#">
                      <i class="fa fa-share"></i> <span>Stock Management</span>
                      <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="treeview-menu <?php if( $this->uri->segment(2)=='' || $this->uri->segment(2)=='categories'  ){echo'active';}?>">
                      <?php if($this->session->userdata('user_role')==1):?>
                      <li class="">
                          <a href="finance"><i class="fa fa-link"></i> <span>Finance</span></a>
                      </li>
                      <?php endif;?>
                      <li class="">
                          <a href="product"><i class="fa fa-link"></i> <span>Stock  List</span></a>
                      </li>
                      <li class="<?php if($this->uri->segment(1)=='admin'){echo'active';}?>"><a href="admin"><i class="fa fa-link"></i> <span>Order List</span></a></li>



                      <li class="<?php if($this->uri->segment(2)=='categories'){echo'active';}?>"><a href="admin/categories"><i class="fa fa-link"></i> Categories</a></li>

                  </ul>
              </li>
            <?php if($this->session->userdata('user_role')==1):?>
             <li class="<?php if($this->uri->segment(2)=='user-management'){echo'active';}?>" ><a href="admin/user-management"><i class="fa fa-link"></i> <span>Admin Management</span></a></li>
          	<?php endif;?>
          </ul><!-- /.sidebar-menu -->