 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 $todaytotal=0;
 $todayq=0;
 foreach ($query as $key => $row) {
     $todaytotal+= $row['total_amount'];
     $todayq+= $row['quantity'];
 }
 ?>

 <div id="reload">
     <?=form_open("finance/search",'')?>
     <section class="row smallsidepadding smallpadding">
         <div class="col-lg-3 col-xs-6">
             <!-- small box -->
             <div class="small-box bg-teal">
                 <div class="inner">

                     <h3><?=count($query)?></h3>
                     <p>Voucher</p>
                 </div>
                 <div class="icon">
                     <i class="fa fa-file-text-o"></i>
                 </div>

             </div>
         </div>

         <div class="col-lg-3 col-xs-6">
             <!-- small box -->
             <div class="small-box bg-aqua">
                 <div class="inner">
                    <h3><?=number_format($todaytotal)?></h3>
                     <p>Total Amount</p>
                 </div>
                 <div class="icon">
                     <i class="fa fa-bar-chart"></i>
                 </div>

             </div>
         </div>

         <div class="col-lg-3 col-xs-6">
             <!-- small box -->
             <div class="small-box bg-aqua">
                 <div class="inner">
                     <h3><?=$todayq?></h3>
                     <p>Total Quantity</p>
                 </div>
                 <div class="icon">
                     <i class="fa fa-unsorted"></i>
                 </div>

             </div>
         </div>

         <div class="col-md-3">
             <div class="form-group">
                 <label>Date range button:</label>

                 <div class="input-group">
                     <button type="button" class="btn btn-default pull-right" id="daterange-btn">
                    <span>
                      <i class="fa fa-calendar"></i> Date range picker
                    </span>
                         <input type="hidden" value="" id="start_date" name="start_date">
                         <input type="hidden" value="" id="end_date" name="end_date">
                         <i class="fa fa-caret-down"></i>
                     </button>
                 </div>
                 <div style='margin-top:3px;'>
                     <?=form_submit("Save","Search","class='btn btn-primary  '")?>
                 </div>

             </div>
         </div>

     </section>
     <?=form_close();?>
         <div class="clearfix"></div>


     <div class="box-body">

             <div class="table-responsive">
                  <table id="hometbl" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                          <th>No</th>
                          <th>Image</th>
                          <th>Product Name</th>
                         <th>Color</th>
                          <th>Item Code</th>
                          <th>Qty</th>
                          <th>Total Amount</th>
                        </tr>
                    </thead>
                    <tbody>

                      <?php
                      $no=1;

                      foreach ($query2 as $key => $row) :
                        $send="unsend";
                       if($row["situation"]==1)
                       {
                        $send="send";
                      }
                      ?>
                      <tr id="row-<?=$row["t_id"]?>" class="<?=$send?>"  >
                        <td><?=$no++?></td>

                          <td>
                              <img width="50" src="upload/<?php if ($this->main_model->getName(@$row['product_id'],'img')==null){echo 'default-product.jpg';}elseif($this->main_model->getName(@$row['product_id'],'img')=='OTS'){echo "out-of-stock.jpg";}else{echo $this->main_model->getName(@$row['product_id'],'img');}?>" class="img-responsive">

                          </td>
                        <td>
                            <?=$this->main_model->getName(@$row["product_id"],'pname')?>
                        </td>
                          <td>
                             <?=@$row['color']?>
                          </td>
                          <td><?=$this->main_model->getName(@$row["product_id"],'pcode')?></td>
                       
                        <td>
                          <?=@$row["quantity"]?>
                        </td>
                          <td>
                              <?=$row["total_amount"]?>
                          </td>



                      </tr>
                    <?php endforeach;?>
                    </tfoot>
                  </table>
                  </div>
                    <?php
                    if($this->session->userdata('user_role')==1||$this->session->userdata('user_role')==2):
                        ?>
                        <div class="box-footer">
                            <a href="admin/add-order" class="btn btn-sm btn-info btn-flat pull-left">Add New</a>
                        </div>
                        <?php
                    endif;
                    ?>
                </div><!-- /.box-body -->
 </div>