 <div class="container bigpadding">

 <div class="col-md-6 col-md-push-1 smalltopmargin">
  <?php
  if (!empty($this->uri->segment(3))) :
  ?>
<?=form_open_multipart("transfer/edit-transfer-process/".$this->uri->segment(3),'')?>
<?php else:?>
    <?=form_open_multipart("transfer/add-transfer-process",'')?>
<?php endif;?>

<section class="row">
     <div class="col-md-3 smallpadding midsidepadding">
         <?=form_label('Delivery Name:')?>
     </div>
     <div class="col-md-9">
         <div class="form-group">
             <select name="delivery_name" id="delivery_name" data-placeholder="Choose one" class="chosen-select" tabindex="2" required>
                 <option value=""></option>
                 <?php
                 $this->db->order_by('first_name','DESC');
                 $this->db->where('user_role',3);
                 $q=$this->db->get('users_tbl')->result_array();
                 foreach($q as $key=>$deli):
                 ?>
                     <option value="<?=$deli['t_id']?>"><?=$deli['first_name']?> <?=$deli['last_name']?></option>
                 <?php
                 endforeach;
                 ?>
             </select>
         </div>
     </div>
     </section>

     <section class="row">
     <div class="col-md-3 smallpadding midsidepadding">
         <?=form_label('Type :')?>
     </div>
     <div class="col-md-9">
         <div class="form-group">
             <select name="product_name[]" id="product_name" data-placeholder="Choose one" class="chosen-select" tabindex="2" required>
                 <option value=""></option>
                 <?php

                 $query=$this->db->get('product_tbl')->result_array();
                 foreach($query as $key=>$val):
                 ?>
                     <option value="<?=$val['t_id']?>"><?=$val['name']?></option>
                 <?php
                 endforeach;
                 ?>
             </select>
         </div>
     </div>
     </section>
     <div id="item_list">

     </div>



        <input id="product_url" type="hidden" value="<?=base_url()?>transfer/product_detail">
      <div class="modal-footer">              
         <?=form_submit("Save","Save","class='btn btn-primary'")?>
      </div>


 
</div>
        
      
  <div class="col-md-2">
  </div>
</div>
<?=form_close();?>
