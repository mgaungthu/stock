<?php
$id = urldecode($this->uri->segment(3));
$this->db->order_by('color',"DESC");
$this->db->where('product_id',$id);
$get=$this->db->get('color_tbl')->result_array();

    ?>
<input id="RemainUrl" type="hidden" value="<?=base_url()?>transfer/remaining_items">
    <div>
        <section class="row">
            <div class="col-md-3">

            </div>
            <div class="col-md-9 text-center">
                <img class="img-responsive smallpadding" width="150" src="upload/<?php if ($this->main_model->getName($id,'img')==null){echo 'default-product.jpg';}elseif($this->main_model->getName($id,'img')=='OTS'){echo "out-of-stock.jpg";}else{echo $this->main_model->getName($id,'img');}?>"  />

            </div>

        </section>
        <section class="row">
        <div class="col-md-3 smallpadding midsidepadding">
            <?=form_label('Color :')?>
        </div>
        <div class="col-md-5">
            <div class="form-group">
                <select name="color[]" id="color" data-placeholder="Choose Color" class="chosen-select" tabindex="2" required>
                    <option value="0"></option>
                    <?php foreach($get as $key=>$colo): ?>
                        <option value="<?=$colo["color"]?>"><?=$colo["color"]?></option>
                        <?php
                    endforeach;
                    ?>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            Remaining Items : <span id="remainItem"></span>
        </div>
        </section>
        <section class="row">
            <div class="col-md-3 smallpadding midsidepadding">
                <?=form_label('Quantity :')?>
            </div>
            <div class="col-md-9">
                <div class="form-group">
                    <input type="number" name="quantity[]" value="<?=@$row["quantity"]?>" placeholder='Quantity' class='form-control' required>
                </div>
            </div>
        </section>
    </div>




<!--        <div class="col-md-3 smallpadding midsidepadding">-->
<!---->
<!--        </div>-->
<!--        <div class="col-md-9">-->
<!--            <div class="form-group">-->
<!--                <select name="quantity[]" id="quantity" data-placeholder="Choose Quanitity" class="chosen-select" tabindex="2" required>-->
<!--                    <option value="0"></option>-->
<!--                    --><?php
//                        for ($x = 0; $x <= $colo["quantity"] ; $x++):
//                        ?>
<!--                        <option value="--><?//=$x?><!--">--><?//=$x?><!--</option>-->
<!--                        --><?php
//                    endfor;
//                    ?>
<!--                </select>-->
<!--            </div>-->
<!--        </div>-->
<!--        <span  id="Remove_btn"><i class="fa  fa-minus"></i> </span>

