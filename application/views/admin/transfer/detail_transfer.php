
<div class="box-body">
	<div class="col-md-12 margin-top">


		<div class="col-md-5">


			<h3> Customer Information </h3>

			<div class="row smallpadding">
				<label class="col-md-3"> <b> Full Name </b></label>
				<label class="col-md-9">  <?=$row["first_name"]?> <?=$row["last_name"]?> </label>
			</div>

			<div class="row smallpadding">
				<label class="col-md-3"> <b> Phone </b></label>
				<label class="col-md-9">   <?=$row["ph_no"]?> </label>
			</div>

			<div class="row smallpadding">
				<label class="col-md-3"> <b> Address </b></label>
				<label class="col-md-9">   <?=$row["address"]?> </label>
			</div>

			<div class="row smallpadding">
				<label class="col-md-3"> <b> Notepad </b></label>
				<label class="col-md-9">   <?=$row["note_pad"]?> </label>
			</div>

			<div class="row smallpadding">
				<label class="col-md-3"> <b> City </b></label>
				<label class="col-md-9">   <?=$row["city"]?> </label>
			</div>

			<div class="row smallpadding">
				<label class="col-md-3"> <b> Order Date  </b></label>
				<label class="col-md-9">   <?=date_time($row["create_date"])?> </label>
			</div>

			<div class="row smallpadding">
				<label class="col-md-3"> <b> Status  </b></label>
				<label class="col-md-9">   <?=situation($row["situation"])?> </label>
			</div>


			<div class="row smallpadding">
				<label class="col-md-3"> <b> Deliverer Name </b></label>
				<label class="col-md-9">   <?=$this->main_model->delivery_name($row["delivery_name"])?> </label>

			</div>

			<?php
			if($this->session->userdata("user_role")==1):
				?>
				<a class="btn btn-primary btn-flat" href="admin/edit-order/<?=$row["t_id"]?>">Edit</a>
				<?php
			endif;
			?>
			<a class="btn btn-success btn-flat" href="admin/">Complete</a>
		</div>

		<div class="col-md-7">
			<h3> Product Information</h3>
			<?=form_open("admin/print-job/".$this->uri->segment(3),'')?>
			<?php
			$this->db->where('order_id',$order_id);
			$val=$this->db->get('order_items_tbl')->result_array();
			foreach ($val as $img):
				?>
				<section class="row smallpadding item-image">
					<div class="col-md-3">
						<img class="img-responsive" src="upload/<?php if ($this->main_model->getName($img['product_id'],'img')==null){echo 'default-product.jpg';}elseif($this->main_model->getName($img['product_id'],'img')=='OTS'){echo "out-of-stock.jpg";}else{echo $this->main_model->getName($img['product_id'],'img');}?>" title="<?= $img['product_id']?>" />

					</div>
					<div class="col-md-8 text-left">
						<label class="col-md-4 smallpadding"> <b> Product Name </b></label>
						<label class="col-md-8 smallpadding">  <?=$this->main_model->getName($img["product_id"],'pname')?> </label>
						<label class="col-md-4 smallpadding"> <b> Item Code </b></label>
						<label class="col-md-8 smallpadding">	<?=$this->main_model->getName($img["product_id"],'pcode')?> </label>
						<label class="col-md-4 smallpadding"> <b> Quantity </b></label>
						<label class="col-md-8 smallpadding">	<?=$img["quantity"]?> </label>
						<label class="col-md-4 smallpadding"> <b> Color </b></label>
						<label class="col-md-8 smallpadding">	<?=$img["color"]?> </label>
						<label class="col-md-4 smallpadding"> <b> Product Type </b></label>
						<label class="col-md-8 smallpadding">	<?=$this->main_model->getName($img["product_id"],'ptype')?> </label>
						<label class="col-md-4 smallpadding"> <b> Category </b></label>
						<label class="col-md-8 smallpadding">	<?=$this->main_model->getName($img["product_id"],'cate')?> </label>
						<label class="col-md-4 smallpadding"> <b> Selling Price </b></label>
						<label class="col-md-8 smallpadding">	<input type="text"  no="<?=$img["quantity"]?>" value="<?=$this->main_model->getName($img["product_id"],'price')?>" name="sellPrice[]" class="form-control price" required>  </label>
					</div>
				</section>
				<?php
			endforeach;
			?>
			<label class="col-md-4 smallpadding"> <b> Total Amount </b></label>
			<label class="col-md-8 smallpadding">	<input type="text" class="form-control total" readonly>  </label>
			<label class="col-md-4 smallpadding"> <b>  Method </b></label>
			<div class="col-md-8 smallpadding">
				<?php
				$options = array(0=>'Choose One','Cash'=>"Cash",'Deposit'=>"Deposit");
				?>
				<?=form_dropdown('paymethod', $options,'',' class="form-control method"  ');?>
			</div>
			<div id="Cash" class="methods" style="display: none;" >
				<label class="col-md-4 smallpadding"> <b> Order Discount </b></label>
				<label class="col-md-8 smallpadding">	<input type="text" value="" name="discount" class="form-control">  </label>
				<label class="col-md-4 smallpadding"> <b> Pay Amount </b></label>
				<label class="col-md-8 smallpadding">	<input type="text" name="payamount" class="form-control">  </label>
			</div>
			<div id="Deposit" class="methods" style="display: none;">
				<label class="col-md-4 smallpadding"> <b> Order Discount </b></label>
				<label class="col-md-8 smallpadding">	<input type="text" value="" name="depdiscount" class="form-control">  </label>
				<label class="col-md-4 smallpadding"> <b> Deposit </b></label>
				<label class="col-md-8 smallpadding">	<input type="text" name="deposit" class="form-control">  </label>
			</div>

			<?=form_submit("Save","Get Voucher","class='btn btn-primary'")?>
			<?=form_close()?>
		</div>

	</div>

</div>

