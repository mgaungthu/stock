 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
<div class="box">
    <div class="box-body">
        <table id="stockList" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>No</th>
                <th>Product Status</th>
                <th>Category</th>
                <th>Image</th>
               <th>Delivery Name</th>
                <th>Name</th>
                <th>Color</th>
                <th>Code</th>
                <th>Transfered Quantity</th>
                <th>Creat Date</th>
                <th>Who Create</th>
                <?php
                if($this->session->userdata('user_role')==1):
                ?>
                <th>Action</th>
                <?php
                endif
                ?>
            </tr>
            </thead>

            <tbody>

            <?php
            $no=1;
            foreach($query as $key=>$row):
            ?>
            <tr id="row-<?=$row['t_id']?>">
                <td><?=$no++?></td>
                <td>
                <?php if($row['p_type']):?>
                <?=p_type($row['p_type'])?>
                <?php else:?>
                -
                <?php endif;?>
                </td>
                <td>
                    <?php if($row['category']):?>
                        <?=$this->main_model->category($row['category'])?>
                    <?php else:?>
                        -
                    <?php endif;?>
                </td>
                <td><img width="50" src="upload/<?php if ($this->main_model->imageName($row['image_name'])==null){echo 'default-product.jpg';}elseif($this->main_model->imageName($row['image_name'])=='OTS'){echo "out-of-stock.jpg";}else{echo $this->main_model->imageName($row['image_name']);}?>" class="img-responsive"></td>
                <td><?=$this->main_model->delivery_name($row['delivery_name'])?></td>
                <td>
                    <?php if($row['name']):?>
                        <?=$row['name']?>
                    <?php else:?>
                        -
                    <?php endif;?>
                </td>
                <td>
                    <?php if($row['color']):?>
                        <?=$row['color']?>
                    <?php else:?>
                        -
                    <?php endif;?>
                </td>
                <td>
                    <?php if($row['item_code']):?>
                        <?=$row['item_code']?>
                    <?php else:?>
                        -
                    <?php endif;?>
                </td>
                <td><?=$row['quantity']?></td>
                <td><?=date_time($row['creat_date'])?></td>
                <td><?=$row['who_created']?></td>
                <?php
                if($this->session->userdata('user_role')==1):
                    ?>
                <td>
                    <?php
                    if($this->main_model->imageName($row['image_name'])!='OTS'):
                        ?>

                    <a  data-toggle="tooltip" title="More Info" href="transfer/edit-transfer/<?=$row["t_id"]?>"><i class="fa fa-file-text-o"></i> </a>
                    <?php endif; ?>
                        <a data-toggle="tooltip" title="Delete" href="transfer/delete-transfer" id="<?=$row["t_id"]?>" class="delete_transfer"><i class="fa fa-trash-o"></i> </a>
                </td>
                <?php
                endif;
                ?>
            </tr>
            <?php
            endforeach;
            ?>
            </tbody>
        </table>
    </div><!-- /.box-body -->
    <?php
    if($this->session->userdata('user_role')==1||$this->session->userdata('user_role')==2):
    ?>
    <div class="box-footer">
        <a href="transfer/add-new-transfer" class="btn btn-primary pull-left">Add New</a>
    </div>
    <?php
    endif;
    ?>
</div>