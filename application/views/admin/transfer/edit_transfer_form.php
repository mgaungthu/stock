<div class="container bigpadding">

    <div class="col-md-6 col-md-push-1 smalltopmargin">


            <?=form_open_multipart("transfer/edit-transfer-process/".$this->uri->segment(3),'')?>

<section class="row">
     <div class="col-md-3 smallpadding midsidepadding">
         <?=form_label('Delivery Name:')?>
     </div>
     <div class="col-md-9">
         <div class="form-group">
             <select name="delivery_name" id="delivery_name" data-placeholder="Choose one" class="chosen-select" tabindex="2" required>
                 <option value=""></option>
                 <?php
                 $this->db->order_by('first_name','DESC');
                 $this->db->where('user_role',3);
                 $q=$this->db->get('users_tbl')->result_array();
                 foreach($q as $key=>$deli):
                 ?>
                     <option value="<?=$deli['t_id']?>" <?php if($deli['t_id']==$row['delivery_name']){echo 'selected';} ?> ><?=$deli['first_name']?> <?=$deli['last_name']?></option>
                 <?php
                 endforeach;
                 ?>
             </select>
         </div>
     </div>
     </section>

        <section class="row">
            <div class="col-md-3 smallpadding midsidepadding">
                <?=form_label('Type :')?>
            </div>
            <div class="col-md-9">
                <div class="form-group">
                    <select name="product_name" id="product_name" data-placeholder="Choose one" class="chosen-select" tabindex="2" required>
                        <option value=""></option>
                        <?php

                        $query=$this->db->get('product_tbl')->result_array();
                        foreach($query as $key=>$val):
                            ?>
                            <option value="<?=$val['t_id']?>" <?php if($val['t_id']==$row['product_id']){echo 'selected';} ?>><?=$val['name']?></option>
                            <?php
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>
        </section>
        <div id="item_list">

            <input id="RemainUrl" type="hidden" value="<?=base_url()?>transfer/remaining_items">
            <div>
                <section class="row">
                    <div class="col-md-3 smallpadding midsidepadding">
                        <?=form_label('Color :')?>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <select name="color" id="color" data-placeholder="Choose Color" class="chosen-select" tabindex="2" required>
                                <?php
                            $this->db->order_by('color',"DESC");
                            $this->db->where('product_id',$row['product_id']);
                            $get=$this->db->get('color_tbl')->result_array();
                                            foreach($get as $op):
                                ?>
                              <option value="<?=$op["color"]?>" <?php if($op['color']==$row['color']){echo 'selected';} ?>   ><?=$op["color"]?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        Remaining Items :
                        <span id="remainItem">
                        <?php
                        $this->db->order_by('color',"DESC");
                        $this->db->where('product_id',$row['product_id']);
                        $this->db->where('color',$row['color']);
                        $r=$this->db->get('color_tbl')->row_array();
                        echo $r['quantity'];
                        ?>
                            </span>
                    </div>
                </section>
                <section class="row">
                    <div class="col-md-3 smallpadding midsidepadding">
                        <?=form_label('Quantity :')?>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <input type="number" name="quantity" value="<?=$row['quantity']?>" placeholder='Quantity' class='form-control' required>
                        </div>
                    </div>
                </section>



        </div>
        </div>
        <input id="product_url" type="hidden" value="<?=base_url()?>transfer/product_detail">
        <div class="modal-footer">
            <?=form_submit("Save","Save","class='btn btn-primary'")?>
        </div>






    <div class="col-md-2">
    </div>
</div>
<?=form_close();?>
