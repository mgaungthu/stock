 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>
 <div id="reload">

<br>
     <?php if($user_role==1):?>
         <a style="margin-left: 10px" class="btn btn-sm btn-primary" id="ckAll">Select All</a>
         <a style="margin-left: 10px;display: none;" class="btn btn-sm btn-danger" id="deckAll">Deselect All</a>
     <span style="display: none;" id="loadinContent">
         Completed
     </span>
 <a link="<?=base_url()?>admin/delete_all" id="delConfirm" style="margin-right: 10px;display: none" class="btn btn-sm btn-danger pull-right" >Delete All</a>
     <?php
     endif;
     ?>
                <div class="box-body">
             <div class="table-responsive">
                  <table id="hometbl" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                          <th> </th>
                          <th>No</th>
                          <th>Image</th>
                          <th>Product Name</th>
                          <th>Color</th>
                          <th>Item Code</th>
                          <th>Full Name</th>
                          <th>Phone Number</th>
                          <th>Address</th>
                          <th>City</th>
                          <th>Qty</th>
                         <th>Order Date</th>
                          <th>Status</th>
                          <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>

                      <?php
                      $no=1;

                      foreach ($query as $key => $row) :
                        $send="unsend";
                       if($row["situation"]==1)
                       {
                        $send="send";
                      }
                      ?>
                      <tr id="row-<?=$row["t_id"]?>" class="<?=$send?>"  >
                          <td><input id="delAll" name="delValue" value="<?=$row["t_id"]?>" onchange="valueChanged()" type="checkbox"></td>
                        <td><?=$no++?></td>

                          <td>
                              <img width="50" src="upload/<?php if ($this->main_model->getName(@$row['product_id'],'img')==null){echo 'default-product.jpg';}elseif($this->main_model->getName(@$row['product_id'],'img')=='OTS'){echo "out-of-stock.jpg";}else{echo $this->main_model->getName(@$row['product_id'],'img');}?>" class="img-responsive">

                          </td>
                          <td>
                              <?=$this->main_model->getName(@$row["Pname"],'pname')?>
                          </td>
                          <td>
                             <?=@$row['color']?>
                          </td>
                          <td><?=$this->main_model->getName(@$row["product_id"],'pcode')?></td>
                         <td>
                          <?=$row["first_name"]?> <?=$row["last_name"]?>
                          </td>

                        <td>
                          <?=$row["ph_no"]?>
                        </td>
                        <td>
                          <?=$row["address"]?>
                        </td>
                        <td>
                          <?=$row["city"]?>
                        </td>

                        <td>
                          <?=@$row["quantity"]?>
                        </td>

                        <td>
                          <?=date_time($row["create_date"])?>
                        </td>
               <?php
               $color="";
              if ($row["situation"]==1)
                {

                  $color = "green";
                }
              elseif ($row["situation"]==0)
                {

                 $color = "teal";
                 }
                elseif ($row["situation"]==2)
                {

                 $color = "blue";
                 }
                elseif ($row["situation"]==3)
                {
                $color = "red";
                }
               elseif ($row["situation"]==4)
                {
                $color = "yellow";
                }
                 else
                 {
                    $color = "normal";
                 }
              ?>
                  <td id="pend-<?=$row["t_id"]?>" class="text-center">

                      <?php
                      if($row["situation"]==5):
                      ?>
                      <span class="situat">
                        P ->
                        <span id="pending_sit"><?=situation($row["pending_sit"])?></span>
                      </span>
                       <?php if ($user_role==1 && $row["situation"]==5 ) :?>
                          <div class="btn-confirm" >
                          <a  id="confirm_sit" href="<?=base_url()?>admin/confirm_sit" pending="<?=$row["pending_sit"]?>" change="<?=$row["t_id"]?>" >Go</a>
                          </div>
                         <?php endif;?>
                    <?php else :?>

                      <span class="situat" id="situat">
                    <select  tid="<?=$row["t_id"]?>" url="<?=base_url()?>admin/situation_update" id="situation" name="situation" class="form-control <?=$color?>">

                        <option value="">Choose One</option>
                        <?php
                        for ($i=0; $i <= 4 ; $i++) :

                        ?>
                       <option value="<?=$i?>" <?php if($row["situation"]==$i){echo "selected";} ?> > <?=situation($i)?></option>

                    <?php endfor ?>
                    </select>

                        </span>
                      <span id="pending_sit"></span>

                      <?php endif;?>
                  </td>

                         <td>
                             <a data-toggle="tooltip" title="More Info" href="admin/more_detail/<?=$row["t_id"]?>"><i class="fa fa-file-text-o"></i> </a>|

                             <a data-toggle="tooltip" title="Edit"  href="admin/edit-order/<?=$row["t_id"]?>"><i class="fa fa-pencil-square-o"></i> </a>|

                             <a data-toggle="tooltip" title="Delete" href="admin/delete-order" id="<?=$row["t_id"]?>" class="delete_order"><i class="fa fa-trash-o"></i> </a>

                        </td>
                      </tr>
                    <?php endforeach;?>
                    </tfoot>
                  </table>
                  </div>
                    <?php
                    if($this->session->userdata('user_role')==1||$this->session->userdata('user_role')==2):
                        ?>
                        <div class="box-footer">
                            <a href="admin/add-order" class="btn btn-sm btn-info btn-flat pull-left">Add New</a>
                        </div>
                        <?php
                    endif;
                    ?>
                </div><!-- /.box-body -->
 </div>