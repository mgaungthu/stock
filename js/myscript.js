 $(document).ready(function() 
    {

        $('#stockList').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
                "aLengthMenu": [
            [25, 50, 100, 200, -1],
            [25, 50, 100, 200, "All"]
        ],
        "iDisplayLength": -1
        });

        $('.chosen-select').chosen();
        $('.chosen-select-deselect').chosen({ allow_single_deselect: true });


        $('body').on('click','.delete_product',function(event)
        {                // $('#loading').show();
            event.preventDefault();
            var id = $(this).attr("id");
            del_url = $(this).attr("href");

            var x =new Object();
            x = {
                'id':id,
            };
            var r=confirm("Are you sure to Delete?")
            if (r==true)
            {
                $.ajax({
                    url: del_url,
                    cache:false,
                    type: 'POST',
                    data: x,
                    success: function(data)
                    {
                        $("#row-"+id).fadeOut('slow');
                    },
                });

            }
        });

        $('body').on('click','.delete_transfer',function(event)
        {                // $('#loading').show();
            event.preventDefault();
            var id = $(this).attr("id");
            del_url = $(this).attr("href");

            var x =new Object();
            x = {
                'id':id,
            };
            var r=confirm("Are you sure to Delete?")
            if (r==true)
            {
                $.ajax({
                    url: del_url,
                    cache:false,
                    type: 'POST',
                    data: x,
                    success: function(data)
                    {
                        $("#row-"+id).fadeOut('slow');
                    },
                });

            }
        });

        $('body').on('click','#deleteCat',function(event)
        {                // $('#loading').show();
            event.preventDefault();
            var id = $(this).attr("val");
            del_url = $(this).attr("url");
            console.log(del_url);
            var x =new Object();
            x = {
                'id':id,
            };
            var r=confirm("All of Child Category will be lost! Are you sure to Delete? ")
            if (r==true)
            {
                $.ajax({
                    url: del_url,
                    cache:false,
                    type: 'POST',
                    data: x,
                    success: function(data)
                    {
                        $(".rowll-"+id).fadeOut('slow');
                    },
                });

            }
        });

        readURL = function (input) {
            if (input.files && input.files[0])
            {
                var reader = new FileReader();

                reader.onload = function (e)
                {
                    $('#preview')
                        .attr('src', e.target.result)
                        .width(175)
                        .height(130);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }


        $('[id=confirm_sit]').on('click',function(event)
        {
            event.preventDefault();
            var pending = $(this).attr("pending");
            url     = $(this).attr("href");
            change = $(this).attr("change");
            $.ajax
            ({
                type: "POST",
                cache:false,
                url: url,
                dataType: 'html',
                data: {pending:pending,change:change},
                success: function(data)
                {
                    if (data)
                    {
                        jQuery("div#pwrapper").fadeIn();
                        jQuery("#pend-"+change).find("span.situat").html(data);
                        jQuery("#pend-"+change).find("#confirm_sit").remove();

                    }
                }
            });

        });

        $('body').on('change','[id=product_name]',function(event){
            var product_id = $(this).val();
            url     = $('[id=product_url]').val();

            var xmlhttp= new XMLHttpRequest();

            xmlhttp.onreadystatechange = function()
            {
                if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
                {
                    document.getElementById('item_list').innerHTML = xmlhttp.responseText;
                    $('.chosen-select').chosen();
                    $('.chosen-select-deselect').chosen({ allow_single_deselect: true });

                }``
            }
            xmlhttp.open('GET',url+'/'+product_id,true);
            xmlhttp.send();
        });


        $("body").on("change", "#color", function()
        {
            var colorName = $(this).val();
            productId=$('#product_name option:selected').val();
            RemainUrl = $('[id=RemainUrl]').val();

            var xmlhttp= new XMLHttpRequest();

            xmlhttp.onreadystatechange = function()
            {
                if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
                {
                    document.getElementById('remainItem').innerHTML = xmlhttp.responseText;


                }
            }
            xmlhttp.open('GET',RemainUrl+'/'+colorName+'/'+productId,true);
            xmlhttp.send();
        });




        QuanityUpdate = function(id,id2,cname)
        {
            var idval = id;
            quantity     = $('#'+idval).find('#quantity').val();
            colorchangename     = $('[id=colorchangename]').val();
            url     = $('[id=updateColorUrl]').val();

            jQuery.ajax({
                type: "POST",
                url: url ,
                data: {idval: idval,quantity:quantity,id2:id2,cname:cname},
                success: function(response) {
                    $('div#ajax_response').fadeIn(100);
                    $('div#ajax_response').html(response);
                    $('div#ajax_response').fadeOut(1000);

                }
            });

        }


        var max_fields      = 5; //maximum input boxes allowed
        var wrapper         = $("#MoreWrapper"); //Fields wrapper
        var add_button      = $("#add_More"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(event){ //on add input button click
            event.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment

                $(wrapper).append('<div><div class="col-md-12"><div class="form-group"><label>Color</label><input placeholder="Color" name="color[]" value="" type="text" class="form-control"></div></div><div class="col-md-12"><div class="form-group"><input name="quantity[]" value="" placeholder="Quantity" class="form-control" required="" type="number"></div></div><span id="Remove_btn"><i class="fa fa-trash"></i> </span></div> ');

                //add input box
            }
        });


        $('#add_pname').click(function() {
            var url = $('#MutiProductUrl').val();

            var max_fields      = 5; //maximum input boxes allowed
                charSet =  $.trim('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
                charSetSize = charSet.length;
                id = '';
                randPos = Math.floor(Math.random() * charSetSize);
                id += charSet[randPos];
            $.ajax({
                url: url,
                type: 'POST',
                data: {number:id},
                success: function(html) {
                    if(x < max_fields) {
                        x++;
                        $("#muti_product").append(html);
                        $('.chosen-select').chosen();
                        $('.chosen-select-deselect').chosen({allow_single_deselect: true});
                    }
                }
            });
        });

        mutiproductname = function(id)
        {

            var product_id = $('#productName'+id).val();
            url     = $('[id=product_url]').val();
             itemid = 'item_list'+id;

            var xmlhttp= new XMLHttpRequest();

            xmlhttp.onreadystatechange = function()
            {
                if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
                {
                    document.getElementById(itemid).innerHTML = xmlhttp.responseText;
                    $('.chosen-select').chosen();
                    $('.chosen-select-deselect').chosen({ allow_single_deselect: true });

                }``
            }
            xmlhttp.open('GET',url+'/'+product_id+'/'+'muti'+'/'+id,true);
            xmlhttp.send();

        }

        mutitColor = function(id)
        {
            var colorName = $('#color'+id).val();

            productId =$('#productName'+id).val();
           
            RemainUrl = $('[id=RemainUrl]').val();
            id = 'remainItem'+id;
            var xmlhttp= new XMLHttpRequest();

            xmlhttp.onreadystatechange = function()
            {
                if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
                {
                    document.getElementById(id).innerHTML = xmlhttp.responseText;


                }
            }
            xmlhttp.open('GET',RemainUrl+'/'+colorName+'/'+productId,true);
            xmlhttp.send();
        }



        $("body").on("click","#del_order", function(e){ //user click on remove text
            e.preventDefault();
            var url = $(this).attr('href');
            t_id =$(this).attr('tid');

            var r=confirm("Are you sure want to deleted")
            if(r==true) {
              $.ajax({
                    url     : url,
                    type: 'POST',
                    data: {t_id:t_id},
                    success: function(data)
                    {
                        $('#row-'+t_id).remove();
                    }

                });
            }


        })
        

        $("body").on("click","#Remove_btn", function(e){ //user click on remove text
            e.preventDefault();
            var url = $('#delurl').val();
            t_id =$(this).attr('tid');

            var r=confirm("Are you sure want to deleted")
            if(r==true) {
                $(this).parent('div').remove();
                x--;
                $.ajax({
                    url     : url,
                    type: 'POST',
                    data: {t_id:t_id},
                    success: function(data)
                    {

                    }

                });
            }


        })


        //////////////////


        $('body').on('click','.delete_order',function(event)
          {                // $('#loading').show();                 
                event.preventDefault(); 
                var id = $(this).attr("id"); 
                    del_url = $(this).attr("href"); 
                                   
                 var x =new Object();                             
                x = {
                    'id':id,
                    };
                var r=confirm("Are you sure to Delete?")
                if (r==true) 
                {
                    $(this).closest("tr").fadeOut();
                $.ajax({
                    url: del_url,
                    cache:false,
                    type: 'POST',
                    data: x,
                    success: function(data) 
                    {


                    },              
                });
             
             }
           });

 

        $('body').on('change','[id=situation]',function(event)
          {                               
               
                var sit=$(this).val();  
                    t_id = $(this).attr('tid');
                if (sit==1)
                {
                 $(this).css("background", "#769876");
                 $("#row-"+t_id).removeClass('unsend').addClass('send');
                }
                else if (sit==0)
                {
                $(this).css("background", "#39CCCC");
                $("#row-"+t_id).removeClass('send').addClass('unsend');
                 }
                else if (sit==2)
                {
                $(this).css("background", "#3C8DBC");
                $("#row-"+t_id).removeClass('send').addClass('unsend');
                 }
                else if (sit==3)
                {
                $(this).css("background", "#D73925");
                $("#row-"+t_id).removeClass('send').addClass('unsend');
                 }
                else if (sit==4)
                {
                $(this).css("background", "#660032");
                $("#row-"+t_id).removeClass('send').addClass('unsend');
                 }
                else if(sit==5)
                {
                    $("#row-"+t_id).removeClass('send').addClass('unsend');
                }

                 else
                 {
                    $(this).css("background", "#dddddd");
                    $("#row-"+t_id).removeClass('send').addClass('unsend');
                 }

                 var url_insert = $(this).attr('url');
                     
                     
                  $.ajax
                  ({
                      type: "POST",
                      cache:false,
                      url: url_insert,
                      dataType: 'json',
                      data: {t_id:t_id,sit:sit},
                      success: function(res) 
                        {
                          if (res)
                         {
                            jQuery("div#pwrapper").fadeIn();
                            jQuery("#pend-"+t_id).find("span.situat").html(res.pending);
                             jQuery("#pend-"+t_id).find("span#pending_sit").html(res.pending_sit);
                          }
                        }
                  });  




           });



        $('[id=confirm_sit]').on('click',function(event)
        {
            event.preventDefault();
              var pending = $(this).attr("pending");
                  url     = $(this).attr("href");
                  change = $(this).attr("change");              
                  $.ajax
                  ({
                      type: "POST",
                      cache:false,
                      url: url,
                      dataType: 'html',
                      data: {pending:pending,change:change},
                      success: function(data) 
                        {
                          if (data)
                         {
                            jQuery("div#pwrapper").fadeIn();
                            jQuery("#pend-"+change).find("span.situat").html(data);
                            jQuery("#pend-"+change).find("#confirm_sit").remove();
                             
                          }
                        }
                  });  

        });

        $('#daterange-btn').daterangepicker(
            {
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                startDate: moment().subtract(29, 'days'),
                endDate: moment()
            },
            function (start, end) {
                $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                $('input[id="start_date"]').val(start.format('YYYY-M-D'));
                $('input[id="end_date"]').val(end.format('YYYY-M-D'));
            }
        );


        $('body').on('click','[id=ckAll]',function() {
            if(this) {
                // Iterate each checkbox
                $(':checkbox').each(function() {
                    this.checked = true;
                });
                $('#delConfirm').show();
                $('#deckAll').show();
            }
        });

        $('body').on('click','[id=deckAll]',function() {
            if(this) {
                // Iterate each checkbox
                $(':checkbox').each(function() {
                    this.checked = false;
                });
                $('#deckAll').hide();
                $('#delConfirm').hide();
            }
        });




        $('body').on('click','#callStatusUpdate',function(event)
        {                // $('#loading').show();
            event.preventDefault();
            var id = $(this).attr("tid");
            value = $(this).attr("val");
            $(this).fadeOut();
            $("#showSelect-"+id).find("span#selectedbox").html("loading...");
            $.ajax
            ({
                type: "POST",
                url: "admin/callStatusUpdate",
                data: {id:id,value:value},
                success: function(data)
                {
                    jQuery("#showSelect-"+id).find("span#selectedbox").html(data);
                }
            });


        });
        
        

        $("#dropval li a").click(function(){
                 alert($(this).text());
            $(".btn").text($(this).text());
            $(".btn").val($(this).text());
        });

        

        var sum = 0;
        $(".price").each(function(){
            sum += +$(this).val()* $(this).attr("no");
        });
        $(".total").val(sum);


        $('body').on("change", ".price", function() {
            var sum = 0;
            $(".price").each(function(){
                sum += +$(this).val()* $(this).attr("no");
            });
            $(".total").val(sum);
        });


        $('.method').change(function(){
            $('.methods').hide();
            $('#' + $(this).val()).show();
        });



        $('body').on('click','[id=delConfirm]',function(event) {
            event.preventDefault();

            var searchIDs = $('input:checked').map(function(){

                return $(this).val();

            });
            var arr= searchIDs.get();
                l = arr.length;
                url     = $(this).attr("link");
            $('#loadinContent').html('Loading...!');
            $('#loadinContent').fadeIn();
        $.ajax
        ({
            type: "POST",
            cache:false,
            url: url,
            dataType: 'html',
            data: {arr:arr},
            success: function(data)
            {
                if (data)
                {

                    jQuery('#reload').html(data)
                    $('#loadinContent').html('Complete');

                }
            }

        });

    });


        valueChanged = function ()
        {
            // if($('#delAll').is(":checked"))
            //     $('#delConfirm').show();
            // else
            //     $('#delConfirm').hide();

            var searchIDs = $('input:checked').map(function(){

                return $(this).val();

            });
            var arr= searchIDs.get();
            l = arr.length;
            if(l!=0)
                $('#delConfirm').show();
            else
                $('#delConfirm').hide();
        }








});

 // simple Javascript





